<?php

namespace KiwiCore\Foundation;


class Environment
{
	const DOMAIN_PC = 1;
	const DOMAIN_MOBILE = 2;
	const DOMAIN_ADMIN = 3;
	const DOMAIN_APP = 11;
	const DOMAIN_CONSOLE = 100;

    /**
     * @var int 当前站点域名类型
     */
    public static $domain = "";

    /**
     * @var string 转换别名之前的path
     */
    public static $originalPath = "";

    public static function isAdmin()
    {
        return self::$domain === self::DOMAIN_ADMIN;
    }

    public static function isPC()
    {
        return self::$domain === self::DOMAIN_PC;
    }

    public static function isMobile()
    {
        return self::$domain === self::DOMAIN_MOBILE;
    }

    public static function isWeb()
    {
        return
            self::$domain === self::DOMAIN_PC ||
            self::$domain === self::DOMAIN_MOBILE;
    }

    public static function isHttp()
    {
        return
            self::$domain === self::DOMAIN_PC ||
            self::$domain === self::DOMAIN_MOBILE ||
            self::$domain === self::DOMAIN_ADMIN;
    }

    public static function isConsole()
    {
        return self::$domain === self::DOMAIN_CONSOLE;
    }

    public static function isApp()
    {
        return self::$domain === self::DOMAIN_APP;
    }
}