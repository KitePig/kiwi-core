<?php

use Illuminate\Support\Str;
use KiwiCore\Foundation\Environment;
use KiwiCore\Service\LinkFormat;

if (!function_exists("textDate")) {
    /**
     * @param $date
     * @param $format
     * @return mixed
     */
    function textDate(\Carbon\Carbon $date, $format = 'm-d')
    {
        if ($date->isToday()){
            return '今天';
        } elseif ($date->isYesterday()){
            return '昨天';
        } else {
            return $date->format($format);
        }
    }
}

if (!function_exists("textWeek")) {
    function textWeek(\Carbon\Carbon $date, $format = 'Y-m-d')
    {
        $dateStr = $date->format($format);
        $week = week($date);
        return "$dateStr 周{$week}";
    }
}

if (!function_exists("week")) {
    function week(\Carbon\Carbon $date)
    {
        $map = [
            "日", "一", "二", "三", "四", "五", "六",
        ];
        if ($date->dayOfWeek < 0 || $date->dayOfWeek > 6) {
            return $date->dayOfWeek;
        }
        return $map[$date->dayOfWeek];
    }
}

if (!function_exists("image")) {
    /**
     * @param $path
     * @return mixed
     */
    function image($path)
    {
        if (empty($path)){
            if (function_exists('defaultPic')){
                $path = defaultPic();
            } else {
                $path = '/default.png';
            }
        }
        return app(LinkFormat::class)->fullImage($path);
    }
}

if (!function_exists("cdn")) {
    /**
     * @param string $path
     * @return string
     */
    function cdn($path)
    {
        if (!Str::startsWith($path, "/")) {
            $path = "/$path";
        }
        if (config("kiwi.local_cdn")) {
            if (Environment::isWeb()) {
                return "https://cdn.bootcss.com$path";
            }
        }
        return "https://cdn.bootcss.com$path";
    }
}

if (!function_exists("assetUrl")) {
    /**
     * @param string $path
     * @return string
     */
    function assetUrl($path)
    {
        return strtolower(config("kiwi.scheme.web")) === "https" ?
            secure_asset($path) :
            asset($path);
    }
}

if (!function_exists("getTheUnlabeledString")) {

    /**
     * @param $string
     * @param int $start
     * @param int $length
     * @param string $encoding
     * @return string
     */
    function getTheUnlabeledString($string, $start = 0, $length = 30, $encoding = 'utf-8'){
        if ($string === '' || empty($string)){
            return $string;
        }
        $content = mb_strcut(trim(strip_tags(stripslashes($string))), $start, $length, $encoding);
        foreach (["　", "\r\n", "\r", "\n"] as $special){
            $content = str_replace($special, '', $content);
        }
        return $content;
    }
}

if (!function_exists("getReplaceContent")) {

    /**
     * @param $content
     * @param $keywords
     * @param bool $replace_count
     * @return mixed|null|string|string[]
     */
    function getReplaceContent($content, $keywords, $replace_count = false){
        if ($content === '' || empty($content)){
            return $content;
        }

        foreach (explode("\n", $keywords) as $value){
            @list($key, $val) = explode('=', $value);
            if(empty($key) || empty($val))
                continue;

            if ($replace_count === false){
                $content = str_replace($key, $val, $content);
            } else {
                $pattern = "/$key/";
                $replacement = "$val";
                $content = preg_replace( $pattern, $replacement, $content, (int) $replace_count );
            }
        }
        return $content;
    }
}