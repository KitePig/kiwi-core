<?php

namespace KiwiCore\Repository;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class BaseRepository
{
    /**
     * @return null|Builder
     */
    protected function query()
    {
        return null;
    }

    /**
     * @param Collection $models
     * @return Collection
     */
    protected function formats(Collection $models)
    {
        foreach ($models as $model) {
            $this->format($model);
        }
        return $models;
    }

    /**
     * @param Model|null $model
     * @return Model
     */
    protected function format(Model &$model = null)
    {
        return $model;
    }

    /**
     * @param $id
     * @return Model|null
     */
    public function findById($id)
    {
        $model = $this->query()->find($id);
        return $this->format($model);
    }

    /**
     * @param array $ids
     * @return Collection
     */
    public function findByIds(array $ids)
    {
        return $this->formats($this->query()->find($ids));
    }

    /**
     * @param \Closure $condition
     * @param \Closure|null $pager
     * @param \Closure|null $sorter
     * @param \Closure|null $selector
     * @return Collection
     */
    public function findByCondition(\Closure $condition, \Closure $pager = null, \Closure $sorter = null, \Closure $selector = null)
    {
        $query = $this->query();
        if (!empty($condition)) {
            $query = $condition->call($this, $query);
        }
        if (!empty($sorter)) {
            $query = $sorter->call($this, $query);
        }
        if (!empty($pager)) {
            $query = $pager->call($this, $query);
        }
        if (!empty($selector)) {
            $query = $selector->call($this, $query);
        }
        return $this->formats($query->get());
    }

    /**
     * @param \Closure $condition
     * @return int
     */
    public function countByCondition(\Closure $condition)
    {
        $query = $condition->call($this, $this->query());
        return $query->count();
    }

    /**
     * @param \Closure $condition
     * @return Model
     */
    public function firstByCondition(\Closure $condition)
    {
        $query = $this->query();
        if (!empty($condition)) {
            $query = $condition->call($this, $query);
        }
        $model = $query->first();
        return $this->format($model);
    }
}