<?php

namespace KiwiCore\Repository;


use KiwiCore\Model\ArticleChannel;
use KiwiCore\Repository\Base\CrudQuery;

class ArticleChannelRepository extends BaseRepository
{
    use CrudQuery;

    public function query()
    {
        return ArticleChannel::query();
    }

}