<?php

namespace KiwiCore\Repository;

use KiwiCore\Model\FriendLink;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class FriendLinkRepository extends BaseRepository
{
	use LocalCacheRepository;

	const CACHE_KEY = "friendLink";

	protected function query()
	{
		return FriendLink::query();
	}

	protected function cacheKey()
	{
		return self::CACHE_KEY;
	}

	protected function createItem(Model &$model)
	{
		$item = new \stdClass();
		$item->id = $model->id;
		$item->title = $model->title;
		$item->link = $model->link;
		$item->position = $model->position;
		$item->domain = $model->domain;
		$item->pattern = $model->pattern;
		return $item;
	}

	protected function sort(Collection $models)
	{
		foreach ($models as &$link) {
			if (Str::contains($link->pattern, "**")) {
				$link->weight = PHP_INT_MAX;
			} else {
				$link->weight = count(explode("*", $link->pattern));
			}
		}
		return $models->sortBy("weight");
	}

	public function findByPattern($url, $domain)
	{

        $linkAll = $this->query()->get(); // TODO 使用 LocalCache
        $patterns = $linkAll->where('domain', $domain)->pluck('pattern');
        $bestPattern = $this->findBestPattern($url, $patterns);
        $result = [];
        foreach ($linkAll as $link) {
			if ($domain === $link->domain && $bestPattern == $link->pattern) {
				$result[] = $link;
			}
		}

		return collect($result)->sortByDesc("position")->values()->all();
	}

    private function findBestPattern($url, $patterns)
    {
        foreach ($patterns as $pattern) {
            if ($pattern == $url) {
                return $pattern;
            }
        }
        foreach ($patterns as $pattern) {
            if (preg_match('/^' . str_replace('/', '\/', $pattern) . '$/', $url)) {
                return $pattern;
            }
        }

        return false;
    }
}