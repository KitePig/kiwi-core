<?php

namespace KiwiCore\Repository;

use KiwiCore\Model\Recommend;

class RecommendRepository extends BaseRepository
{
    public function query()
    {
        return Recommend::query();
    }

}