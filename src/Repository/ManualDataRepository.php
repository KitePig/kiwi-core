<?php
namespace KiwiCore\Repository;

use Illuminate\Support\Facades\DB;
use KiwiCore\Model\ManualData;
use KiwiCore\Model\ManualDataMaps;
use KiwiCore\Repository\Base\CrudQuery;

class ManualDataRepository extends BaseRepository
{
    use CrudQuery;

    protected function query()
    {
        return ManualData::query();
    }

    protected function manualDataMapsQuery()
    {
        return ManualDataMaps::query();
    }

    public function manualDataMapsGroup(){
        $manualGroup = $this->manualDataMapsQuery()->select(DB::raw('manual_id, count(manual_id) as manual_count'))->groupBy('manual_id')->get();

        return $manualGroup;
    }

    // 通过data_id 获取数据id
    public function manualDataByDataId($id){
        return $this->manualDataMapsQuery()->where('data_id', $id)->get();
    }

    // 关系创建
    public function manualDataMapsCreate($manualId, array $manualDataIds){
        $dataIds = array_unique($manualDataIds);
        $insert = collect($dataIds)
            ->map(function ($dataId) use ($manualId) {
                return ["manual_id" => $manualId, "data_id" => $dataId];
            })
            ->toArray();
        return $this->manualDataMapsQuery()->insert($insert);
    }

    // 关系删除
    public function manualDataMapsDelete($dataId){
        return $this->manualDataMapsQuery()->where('data_id', $dataId)->delete();
    }

}