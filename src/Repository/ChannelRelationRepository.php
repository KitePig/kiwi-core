<?php

namespace KiwiCore\Repository;


use KiwiCore\Model\ArticleChannel;

class ChannelRelationRepository extends BaseRepository
{
    public function query()
    {
        return ArticleChannel::query();
    }

    public function create($id, array $channels)
    {
        $insert = collect($channels)
            ->map(function ($channel) use ($id) {
                return ["channel" => $channel, "article_id" => $id];
            })
            ->toArray();
        $this->query()->insert($insert);
    }

    public function delete($id)
    {
        $this->query()->where("article_id", $id)->delete();
    }
}