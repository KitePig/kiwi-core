<?php

namespace KiwiCore\Repository;

use KiwiCore\Condition\AntsCollectTaskCondition;
use KiwiCore\Model\AntsCollectTask;
use KiwiCore\Repository\Base\CrudQuery;
use KiwiCore\Repository\BaseRepository;

class AntsCollectTaskRepository extends BaseRepository
{
    use CrudQuery;

    public function query()
    {
        return AntsCollectTask::query();
    }

    // 自动采集
    public function scheduleAutoCollect(){
        return $this->findByCondition(
            AntsCollectTaskCondition::scheduleAutoCollect()
        );
    }

    // 自动发布
    public function scheduleAutoPublished(){
        return $this->findByCondition(
            AntsCollectTaskCondition::scheduleAutoPublished()
        );
    }

}