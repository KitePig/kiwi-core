<?php

namespace KiwiCore\Repository;


use KiwiCore\Foundation\Environment;
use KiwiCore\Model\Article;
use KiwiCore\Repository\Base\CrudQuery;
use Illuminate\Database\Eloquent\Model;

class ArticleRepository extends BaseRepository
{
    use CrudQuery, ArticleRelation;

    public function query()
    {
        return Article::query();
    }

    public function delete($id)
    {
        $this->query()->where("id", $id)->update(["state" => Article::STATE_DELETED]);
    }

    protected function format(Model &$model = null)
    {
        if ($model === null) {
            return $model;
        }
        if (empty($model->image)) {
            if (Environment::isWeb() || Environment::isApp()) {
                $model->image = '/images/default.png';
            }
        } else {
            $model->image = image($model->image);
        }
        return $model;
    }
}