<?php
namespace KiwiCore\Repository;

use KiwiCore\Model\ManualDataMaps;
use KiwiCore\Repository\Base\CrudQuery;

class ManualDataMapsRepository extends BaseRepository
{
    use CrudQuery;

    protected function query()
    {
        return ManualDataMaps::query();
    }

}