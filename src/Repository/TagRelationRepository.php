<?php

namespace KiwiCore\Repository;

use KiwiCore\Model\ArticleTag;
use KiwiCore\Repository\Base\CrudQuery;

class TagRelationRepository extends BaseRepository
{
    use CrudQuery;

    public function query()
    {
        return ArticleTag::query();
    }

    public function create($id, array $tags)
    {
        $tags = array_unique($tags);
        $insert = collect($tags)
            ->map(function ($tag) use ($id) {
                return ["tag" => $tag, "article_id" => $id];
            })
            ->toArray();
        $this->query()->insert($insert);
    }

    public function delete($id)
    {
        $this->query()->where("article_id", $id)->delete();
    }
}