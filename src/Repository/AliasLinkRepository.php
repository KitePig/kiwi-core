<?php

namespace KiwiCore\Repository;


use KiwiCore\Model\AliasLink;
use Illuminate\Database\Eloquent\Model;

class AliasLinkRepository extends BaseRepository implements TableRepository
{
	use LocalCacheRepository;

	const CACHE_KEY = "aliasLink";

	protected function query()
	{
		return AliasLink::query();
	}

	protected function cacheKey()
	{
		return self::CACHE_KEY;
	}

	protected function createItem(Model & $model)
	{
		$item = new \stdClass();
		$item->id = $model->id;
		$item->alias = $model->alias;
		$item->link = $model->link;
		$item->global = $model->global;
		return $item;
	}

	public function globalAlias($link)
	{
		foreach ($this->all() as $aliasLink) {
			if ($aliasLink->link === $link && $aliasLink->global === AliasLink::GLOBAL_REPLACE) {
				return $aliasLink->alias;
			}
		}
		return $link;
	}
}