<?php
namespace KiwiCore\Repository;

use KiwiCore\Foundation\Environment;
use KiwiCore\Model\Tdk;
use KiwiCore\Repository\Base\LocalCacheQuery;
use Illuminate\Database\Eloquent\Model;

class TdkRepository extends BaseRepository
{
    use LocalCacheQuery;

    const CACHE_KEY = "tdk";

    protected function query()
    {
        return Tdk::query();
    }

    protected function cacheKey()
    {
        return self::CACHE_KEY;
    }

    protected function createItem(Model & $model)
    {
        $item = new \stdClass();
        $item->id = $model->id;
        $item->title = $model->title;
        $item->description = $model->description;
        $item->keywords = $model->keywords;
        $item->domain = $model->domain;
        $item->pattern = $model->pattern;
        return $item;
    }

    public function firstByPattern($url, $domain)
    {
        $tdkAll = $this->query()->get(); // TODO 使用 LocalCache
        $patterns = $tdkAll->where('domain', $domain)->pluck('pattern');
        $bestPattern = $this->findBestPattern($url, $patterns);

        // 如果移动端 没有找到tdk 可以使用pc的tdk
        if (empty($bestPattern) && Environment::DOMAIN_MOBILE == $domain){
            $patterns = $tdkAll->where('domain', Environment::DOMAIN_PC)->pluck('pattern');
            $bestPattern = $this->findBestPattern($url, $patterns);
            #todo: 再优化
            if ($bestPattern) {
                foreach ($tdkAll as $tdk) {
                    if ($bestPattern === $tdk->pattern) {
                        return $tdk;
                    }
                }
            }
            return null;
        }

        if ($bestPattern) {
            foreach ($tdkAll as $tdk) {
                if ($tdk->domain === $domain && $bestPattern === $tdk->pattern) {
                    return $tdk;
                }
            }
        }

        return null;
    }

    private function findBestPattern($url, $patterns)
    {
        foreach ($patterns as $pattern) {
            if ($pattern == $url) {
                return $pattern;
            }
        }
        foreach ($patterns as $pattern) {
            if (preg_match('/^' . str_replace('/', '\/', $pattern) . '$/', $url)) {
                return $pattern;
            }
        }

        return false;
    }
}