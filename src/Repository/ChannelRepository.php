<?php

namespace KiwiCore\Repository;


use KiwiCore\Model\Channel;
use KiwiCore\Repository\Base\CrudQuery;

class ChannelRepository extends BaseRepository
{
    use CrudQuery;

    public function query()
    {
        return Channel::query();
    }

    public function all()
    {
        /**
         * 使用sortBy("position")->sortBy("parent")不行
         * 因为parent重复，而且php底层为不稳定排序，所以position会被打乱
         *
         * 以下两种方式都可以
         */
        $channels = $this->query()->get()
            ->groupBy("parent")
            ->flatMap(function ($values) {
                return collect($values)->sortBy("position")->values();
            });

        return $channels;
    }
}