<?php
namespace KiwiCore\Condition;

use Illuminate\Database\Eloquent\Builder;

class ChannelRelationCondition
{
    use Sorter, Pager, Selector;

    public static function channelByArticleId($article_id)
    {
        return function (Builder $query) use ($article_id){
            return $query->select('channel')->where('article_id', $article_id);
        };
    }


}