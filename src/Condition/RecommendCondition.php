<?php

namespace KiwiCore\Condition;

use Illuminate\Database\Eloquent\Builder;

class RecommendCondition
{
    use Sorter, Pager, Selector;

    /**
     * all
     * @return \Closure
     */
	public static function all()
	{
		return function (Builder $query){
			return $query;
		};
	}

	public static function grouping($group)
	{
		return function (Builder $query) use ($group) {
			return $query->where('grouping', $group);
		};
	}


}