<?php
namespace KiwiCore\Condition;

use Illuminate\Database\Eloquent\Builder;

class ManualDataCondition
{
    use Sorter, Pager, Selector;

    /**
     * all
     * @return \Closure
     */
    public static function all()
    {
        return function (Builder $query) {
            return $query;
        };
    }

    /**
     * manualColumns
     * @param $type
     * @return \Closure
     */
    public static function manualColumns($type)
    {
        return function (Builder $query) use ($type) {
            return $query->where('type', ucfirst($type));
        };
    }

    /**
     * @param $name
     * @return \Closure
     */
    public static function name($name)
    {
        return function (Builder $query) use ($name) {
            return $query->where('name', $name);
        };
    }

}