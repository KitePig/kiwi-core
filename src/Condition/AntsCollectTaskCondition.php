<?php
namespace KiwiCore\Condition;

use KiwiCore\Model\AntsCollectTask;
use Illuminate\Database\Eloquent\Builder;
use KiwiCore\Condition\Pager;
use KiwiCore\Condition\Selector;
use KiwiCore\Condition\Sorter;

class AntsCollectTaskCondition
{
    use Sorter, Pager, Selector;

    public static function all()
    {
        return function (Builder $query) {
            return $query->where('state', '!=', AntsCollectTask::STATE_DELETE);
        };
    }

    public static function autoPublishedType($type)
    {
        return function (Builder $query) use ($type){
            return $query->where('auto_published_type', $type);
        };
    }

    public static function scheduleAutoCollect()
    {
        return function (Builder $query) {
            return $query
                ->where('auto_collect_type', AntsCollectTask::AUTO_COLLECT_CUSTOM)
                ->where('state', '!=', AntsCollectTask::STATE_DELETE);
        };
    }

    public static function scheduleAutoPublished()
    {
        return function (Builder $query) {
            return $query
                ->where('auto_published_type', AntsCollectTask::AUTO_PUBLISHED_CUSTOM)
                ->where('state', '!=', AntsCollectTask::STATE_DELETE);
        };
    }

}