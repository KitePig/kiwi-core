<?php

namespace KiwiCore\Condition;

use Illuminate\Database\Eloquent\Builder;

class TagCondition
{
	use Sorter, Selector, Pager;

    public static function all()
    {
        return function (Builder $query) {
            return $query;
        };
    }

    /**
     * @param $filter
     * @return \Closure
     */
    public static function byFilter($filter){
        return function (Builder $query) use ($filter) {
            if (isset($filter->title)){
                $query->where('title', 'like', "%$filter->title%");
            }
            return $query;
        };
    }

    public static function search($title)
    {
        return function (Builder $query) use ($title) {
            if (!empty($title)) {
                $query->where("title", "LIKE", "%$title%");
            }
            return $query;
        };
    }

    public static function byName($name)
    {
        return function (Builder $query) use ($name) {
            return $query
                ->where("name", $name);
        };
    }

    public static function byNames(array $name)
    {
        return function (Builder $query) use ($name) {
            return $query
                ->whereIn("name", $name);
        };
    }

    public static function byTitle($title)
    {
        return function (Builder $query) use ($title) {
            return $query
                ->Where("title", $title);
        };
    }
}