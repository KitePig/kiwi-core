<?php
namespace KiwiCore\Condition;

use KiwiCore\Model\AntsCollectData;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use KiwiCore\Model\AntsCollectTask;

class AntsCollectDataCondition
{
    use Sorter, Pager, Selector;

    public static function all()
    {
        return function (Builder $query) {
            return $query;
        };
    }

    public static function taskId($taskId)
    {
        return function (Builder $query) use ($taskId) {
            $query = $query->where('task_id', $taskId);
            return $query;
        };
    }

    public static function state($taskId, $state = null)
    {
        return function (Builder $query) use ($taskId, $state) {
            $query = $query->where('task_id', $taskId);
            if ($state !== null && $state != AntsCollectData::STATE_ALl){
                $query->where('state', $state);
            }

            return $query;
        };
    }

    public static function stateComplete(array $taskIds)
    {
        return function (Builder $query) use ($taskIds){
            $query = $query
                ->where('state', AntsCollectData::STATE_COMPLETE)
                ->whereIn('task_id', $taskIds);

            return $query;
        };
    }

    public static function statistical($taskId)
    {
        return function (Builder $query) use ($taskId) {
            $query = $query->select(DB::raw('state, count(state) as count'))->where('task_id', $taskId)->groupBy('state');

            return $query;
        };
    }

    public static function sourceLinks(array $links)
    {
        return function (Builder $query) use ($links){
            return $query->whereIn('source_link', $links);
        };
    }

}