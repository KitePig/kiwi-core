<?php

namespace KiwiCore\Condition;

use Illuminate\Database\Eloquent\Builder;

class TdkCondition
{
    use Sorter, Pager, Selector;

    /**
     * 所有tdk
     * @return \Closure
     */
	public static function all()
	{
		return function (Builder $query) {
			return $query;
		};
	}

    /**
     * @param $filter
     * @return \Closure
     */
    public static function byFilter($filter){
        return function (Builder $query) use ($filter) {
            if (isset($filter->pattern)){
                $query->where('pattern', $filter->pattern);
            }
            if (isset($filter->domain)){
                $query->where('domain', $filter->domain);
            }
            return $query;
        };
    }
}