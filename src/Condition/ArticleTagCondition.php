<?php
namespace KiwiCore\Condition;

use Illuminate\Database\Eloquent\Builder;

class ArticleTagCondition
{
    use Sorter, Pager, Selector;

    public static function tag($tag)
    {
        return function (Builder $query) use ($tag){
            return $query->where('tag', $tag);
        };
    }

    public static function tags($tags)
    {
        return function (Builder $query) use ($tags){
            return $query->whereIn('tag', $tags);
        };
    }

    public static function articleId($article_id)
    {
        return function (Builder $query) use ($article_id){
            return $query->where('article_id', $article_id);
        };
    }

    public static function articleIds(array $article_ids)
    {
        return function (Builder $query) use ($article_ids){
            return $query->whereIn('article_id', $article_ids);
        };
    }

}