<?php
namespace KiwiCore\Condition;

use Illuminate\Database\Eloquent\Builder;

class ManualDataMapsCondition
{
    use Sorter, Pager, Selector;

    /**
     * @param $manualId
     * @return \Closure
     */
    public static function manualId($manualId)
    {
        return function (Builder $query) use ($manualId) {
            return $query->where('manual_id', $manualId);
        };
    }

    /**
     * @param $dataId
     * @return \Closure
     */
    public static function dataId($dataId)
    {
        return function (Builder $query) use ($dataId) {
            return $query->where('data_id', $dataId);
        };
    }

}