<?php
namespace KiwiCore\Condition;

use Illuminate\Database\Eloquent\Builder;

class ArticleChannelCondition
{
    use Sorter, Pager, Selector;

    public static function channel($channel)
    {
        return function (Builder $query) use ($channel){
            return $query->where('channel', $channel);
        };
    }

    public static function selectByArticleId($article_id)
    {
        return function (Builder $query) use ($article_id){
            return $query->select('name')->where('article_id', $article_id);
        };
    }

    public static function articleId($article_id)
    {
        return function (Builder $query) use ($article_id){
            return $query->where('article_id', $article_id);
        };
    }

    public static function articleIds(array $article_ids)
    {
        return function (Builder $query) use ($article_ids){
            return $query->whereIn('article_id', $article_ids);
        };
    }


}