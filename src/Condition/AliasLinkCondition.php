<?php
namespace KiwiCore\Condition;

use Illuminate\Database\Eloquent\Builder;

class AliasLinkCondition
{
    use Sorter, Pager, Selector;

    /**
     * all
     * @return \Closure
     */
    public static function all()
    {
        return function (Builder $query) {
            return $query;
        };
    }

    /**
     * @param $filter
     * @return \Closure
     */
    public static function byFilter($filter){
        return function (Builder $query) use ($filter) {
            if (isset($filter->alias)){
                $query->where('alias', $filter->alias);
            }
            if (isset($filter->link)){
                $query->where('link', $filter->link);
            }
            return $query;
        };
    }
}