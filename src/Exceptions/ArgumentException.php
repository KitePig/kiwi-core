<?php

namespace KiwiCore\Exceptions;

use Exception;

/**
 * Class ArgumentException 自定义参数异常
 * @package App\Exception
 */
class ArgumentException extends Exception
{
    public function renderJson()
    {
        return [
            "status" => 422,
            "message" => $this->getMessage(),
        ];
    }
}