<?php

namespace KiwiCore\Exceptions;

use Exception;
/**
 * Class AppException 服务器内部异常
 * @package App\Exception
 */
class AppException extends Exception
{
	public function renderJson()
	{
		return [
			"status" => 500,
			"message" => "Server Error",
		];
	}
}