<?php

namespace KiwiCore\Http\Middleware;


use KiwiCore\Model\AliasLink;
use Closure;
use KiwiCore\Foundation\Environment;
use KiwiCore\Repository\AliasLinkRepository;
use KiwiCore\Service\MemberAccessor;
use Illuminate\Http\Request;

/**
 * Class UrlAlias
 * @package App\Http\Middleware
 * @remark at Kernel.php, must in $middleware, cannot in 'web'
 */
class UrlAlias
{
	/**
	 * @var AliasLinkRepository
	 */
	private $aliasLinkRepository;

	public function __construct(AliasLinkRepository $aliasLinkRepository)
	{
		$this->aliasLinkRepository = $aliasLinkRepository;
	}

    public function handle(Request $request, Closure $next)
    {
        if (!Environment::isWeb()) {
            return $next($request);
        }

        // Uri 可以接受问号后参数
        #$path = $request->path();
        $path = $request->getRequestUri();

        $newPath = "";
        foreach ($this->aliasLinkRepository->all() as $link) {
            if ($this->trimSlash($path) === $this->trimSlash($link->alias)) {
                $newPath = $link->link;
//                暂时去掉
//                Logger::debug("found alias old:$path new:$newPath");

                if ($link->global === AliasLink::GLOBAL_REDIRECT) {
                    return redirect($newPath);
                }
                break;
            }
        }
        if (empty($newPath)) {
            return $next($request);
        }

        // change request path info, FindRouter only need path info
        $accessor = app(MemberAccessor::class);
        $accessor->set($request, "pathInfo", $newPath);

        // original path
        Environment::$originalPath = $path;

        return $next($request);
    }

	private function trimSlash($path)
	{
		return trim($path, "/");
	}
}