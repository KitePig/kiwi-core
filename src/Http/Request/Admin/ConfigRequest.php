<?php

namespace KiwiCore\Http\Request\Admin;


use KiwiCore\Http\Request\Request;

class ConfigRequest extends Request
{
	public function rules()
	{
		return [
			"content" => "required|string",
		];
	}

	public function value()
	{
		return $this->input("content");
	}
}