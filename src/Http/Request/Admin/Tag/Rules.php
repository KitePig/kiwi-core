<?php

namespace KiwiCore\Http\Request\Admin\Tag;

use KiwiCore\Model\Tag;

trait Rules
{
	public function rules()
	{
		return [
			"name" => "required|max:50",
			"title" => "required|string|max:10",
		];
	}

	protected function _model(Tag $model)
	{
		$name = $this->input("name");
		$model->name = str_replace(" ", "-", strtolower($name));
		$model->title = $this->input("title");
		return $model;
	}
}