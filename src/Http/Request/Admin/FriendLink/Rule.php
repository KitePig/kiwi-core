<?php

namespace KiwiCore\Http\Request\Admin\FriendLink;


use KiwiCore\Model\FriendLink;

trait Rule
{
	public function rules()
	{
		return [
			"title" => "required|string|max:10",
			"link" => "required|string|max:100",
			"position" => "required|integer",
			"pattern" => "required|string|max:100",
			"domain" => "required|integer|in:1,2"
		];
	}

	public function messages()
	{
		return [
			"name.required" => "必须填写名称"
		];
	}

	protected function _model(FriendLink $model)
	{
		$model->title = $this->input("title");
		$model->link = $this->input("link");
		$model->position = intval($this->input("position"));
		$model->pattern = $this->input("pattern");
		$model->domain = intval($this->input("domain"));
		return $model;
	}
}