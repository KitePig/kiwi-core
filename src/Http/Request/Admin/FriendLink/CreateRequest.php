<?php

namespace KiwiCore\Http\Request\Admin\FriendLink;


use KiwiCore\Http\Request\Request;
use KiwiCore\Model\FriendLink;

class CreateRequest extends Request
{
	use Rule;

	public function model()
	{
		return $this->_model(new FriendLink());
	}
}