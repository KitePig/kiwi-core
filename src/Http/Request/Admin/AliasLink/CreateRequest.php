<?php

namespace KiwiCore\Http\Request\Admin\AliasLink;


use KiwiCore\Http\Request\Request;
use KiwiCore\Model\AliasLink;

class CreateRequest extends Request
{
	use Rule;

	public function model()
	{
		return $this->_model(new AliasLink());
	}
}