<?php

namespace KiwiCore\Http\Request\Admin\AliasLink;


use KiwiCore\Model\AliasLink;

trait Rule
{
	public function rules()
	{
		return [
			"alias" => "required|string|max:100",
			"link" => "required|string|max:100",
			"global" => "required|integer|in:0,1",
		];
	}

	public function messages()
	{
		return [
			"name.required" => "必须填写名称"
		];
	}

	protected function _model(AliasLink $model)
	{
		$model->alias = request("alias");
		$model->link = request("link");
		$model->global = request("global");
		return $model;
	}
}