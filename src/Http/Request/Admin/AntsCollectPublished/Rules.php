<?php

namespace KiwiCore\Http\Request\Admin\AntsCollectPublished;

use KiwiCore\Model\AntsCollectPublished;

trait Rules
{
	public function rules()
	{
		return [
			"state" => "required|max:2",
			"title" => "required|max:40",
			"auto_published_cron" => "required|string|max:40",
			"auto_published_rules" => "required|string",
			"exclude_channels" => "max:255",
		];
	}

	protected function _model(AntsCollectPublished $model)
	{
		$model->state = $this->input("state", '2');
		$model->title = $this->input("title");
		$model->auto_published_cron = $this->input("auto_published_cron");
		$model->auto_published_rules = $this->input("auto_published_rules") ? $this->input("auto_published_rules") : '';
		$model->exclude_channels = $this->input("exclude_channels")?:'';
		return $model;
	}
}