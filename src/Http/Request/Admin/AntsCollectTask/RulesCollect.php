<?php

namespace KiwiCore\Http\Request\Admin\AntsCollectTask;

use KiwiCore\Model\AntsCollectTask;

trait RulesCollect
{
	public function rules()
	{
		return [
			"auto_collect_type" => "required|max:2",
			"auto_collect_cron" => "required|string|max:40",
		];
	}

    protected function _model(AntsCollectTask $model)
    {
        $model->auto_collect_type = $this->input("auto_collect_type");
        $model->auto_collect_cron = $this->input("auto_collect_cron");
        return $model;
    }
}