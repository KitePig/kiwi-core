<?php

namespace KiwiCore\Http\Request\Admin\AntsCollectTask;

use KiwiCore\Model\AntsCollectTask;
use KiwiCore\Http\Request\Request;

class UpdatePublishedRequest extends Request
{
	use RulesPublished;

	public function modelPublished(AntsCollectTask $model)
	{
		return $this->_modelPublished($model);
	}
}