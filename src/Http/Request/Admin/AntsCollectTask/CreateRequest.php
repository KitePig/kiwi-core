<?php

namespace KiwiCore\Http\Request\Admin\AntsCollectTask;

use KiwiCore\Http\Request\Request;
use KiwiCore\Model\AntsCollectTask;

class CreateRequest extends Request
{
	use Rules;

	public function model()
	{
		return $this->_model(new AntsCollectTask());
	}
}