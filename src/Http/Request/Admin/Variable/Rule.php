<?php

namespace KiwiCore\Http\Request\Admin\Variable;

use KiwiCore\Model\Variable;

trait Rule
{
	public function rules()
	{
		return [
			"name" => "required|string|max:50",
			"type" => "required|string|max:10",
			"value" => "required|string",
			"description" => "string|max:200",
		];
	}

	public function messages()
	{
		return [
			"name.required" => "必须填写名称"
		];
	}

	protected function _model(Variable $model)
	{
		$model->name = $this->input("name");
		$type = $this->input("type");
		$model->type = in_array($type, ['string', 'int', 'json']) ? $type : 'string';
		$value = $this->input("value");
		
		if ($type == 'json'){
            if (!json_decode($value)){
                throw new \Exception('Json参数错误');
            }
        }
		$model->value = $value;
		$model->description = $this->input("description");
		return $model;
	}
}