<?php

namespace KiwiCore\Http\Request\Admin\ManualData;

use KiwiCore\Http\Request\Request;
use KiwiCore\Model\ManualData;

class CreateRequest extends Request
{
	use Rule;

	public function model()
	{
		return $this->_model(new ManualData());
	}
}