<?php

namespace KiwiCore\Http\Request\Admin\ManualData;

use KiwiCore\Model\ManualData;

trait Rule
{
	public function rules()
	{
		return [
			"type" => "required|string|max:10",
			"name" => "required|string|max:20",
			"title" => "required|string|max:50",
			"description" => "string|max:200",
		];
	}

	public function messages()
	{
		return [
			"type.required" => "必须填写类型",
			"name.required" => "必须填写Name",
			"title.required" => "必须填写名称",
		];
	}

	protected function _model(ManualData $model)
	{
        $model->type = $this->input("type");
        $model->name = $this->input("name");
        $model->title = $this->input("title");
        $model->describe = $this->input("describe");
		return $model;
	}
}