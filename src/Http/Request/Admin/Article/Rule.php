<?php

namespace KiwiCore\Http\Request\Admin\Article;

use KiwiCore\Model\Article;

trait Rule
{
	public function rules()
	{
		return [
			"title" => "required|string|max:50",
			"short_title" => "string|max:15",
			"channel" => "required|string|max:50",
			"image" => "max:100",
			"content" => "required|string",
			"state" => "required|integer|in:0,1,2",
		];
	}

	protected function _model(Article $model)
	{
		$model->title = $this->input("title");
		$model->short_title = $this->nullable("short_title", "");
		$model->channel = $this->nullable("channel", "");
		$model->image = $this->nullable("image", "");
		$model->content = $this->nullable("content", "");
		$model->state = $this->nullable("state", 1);
		return $model;
	}

	public function tags()
	{
		return $this->input("tags", []);
	}

	public function channels()
	{
		return $this->input("channels", []);
	}
}