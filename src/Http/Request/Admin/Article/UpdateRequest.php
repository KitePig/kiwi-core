<?php

namespace KiwiCore\Http\Request\Admin\Article;

use KiwiCore\Http\Request\Request;
use KiwiCore\Model\Article;

class UpdateRequest extends Request
{
	use Rule;

	public function model(Article $model)
	{
		return $this->_model($model);
	}
}