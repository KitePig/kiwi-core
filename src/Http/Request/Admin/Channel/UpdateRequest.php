<?php

namespace KiwiCore\Http\Request\Admin\Channel;

use KiwiCore\Model\Channel;
use KiwiCore\Http\Request\Request;

class UpdateRequest extends Request
{
	use Rules;

	public function model(Channel $model)
	{
		return $this->_model($model);
	}
}