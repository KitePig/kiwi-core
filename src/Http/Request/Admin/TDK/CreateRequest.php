<?php

namespace KiwiCore\Http\Request\Admin\TDK;

use KiwiCore\Http\Request\Request;
use KiwiCore\Model\Tdk;

class CreateRequest extends Request
{
	use Rule;

	public function model()
	{
		return $this->_model(new Tdk());
	}
}