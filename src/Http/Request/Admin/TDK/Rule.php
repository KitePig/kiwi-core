<?php

namespace KiwiCore\Http\Request\Admin\TDK;

use KiwiCore\Model\TDK;

trait Rule
{
	public function rules()
	{
		return [
			"title" => "required|string|max:100",
			"description" => "string|max:200",
			"keywords" => "string|max:100",
			"pattern" => "required|string|max:100",
			"domain" => "required|integer|in:1,2"
		];
	}

	public function messages()
	{
		return [
			"name.required" => "必须填写名称"
		];
	}

	protected function _model(TDK $model)
	{
		$model->title = request("title");
		$model->description = request("description", "");
		$model->keywords = request("keywords", "");
		$model->pattern = request("pattern");
		$model->domain = intval(request("domain"));
		return $model;
	}
}