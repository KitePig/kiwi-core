<?php

namespace KiwiCore\Http\Request\Admin\TDK;

use KiwiCore\Model\TDK;
use KiwiCore\Http\Request\Request;

class UpdateRequest extends Request
{
	use Rule;

	public function model(TDK $model)
	{
		return $this->_model($model);
	}
}