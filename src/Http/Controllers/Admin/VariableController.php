<?php

namespace KiwiCore\Http\Controllers\Admin;

use KiwiCore\Condition\VariableCondition;
use KiwiCore\Http\Controllers\AdminController;
use KiwiCore\Http\Request\Admin\Variable\CreateRequest;
use KiwiCore\Http\Request\Admin\Variable\UpdateRequest;
use KiwiCore\Repository\VariableRepository;
use KiwiCore\Service\CheckById;

class VariableController extends AdminController
{
	use TableTrait, CheckById;

	protected static $columns = [
		["name" => "id", "type" => "id", "title" => "ID", "data" => "id"],
		["name" => "__op", "type" => "op", "title" => "操作", "data" => "__op", "target" => [
			["name" => "update", "title" => "编辑"],
			["name" => "remove", "title" => "删除"],
		]],
        ["name" => "name", "type" => "string", "title" => "name", "data" => "name"],
        ["name" => "type", "type" => "string", "title" => "数据类型", "data" => "type"],
        ["name" => "description", "type" => "string", "title" => "描述", "data" => "description"],
        ["name" => "value", "type" => "string", "title" => "value", "data" => "value"],
	];

	protected static $filters = [
//		["name" => "title"],
	];

	protected $repository;

	protected $tagRelationRepository;

	public function __construct()
	{
		parent::__construct();
		$this->repository = app(VariableRepository::class);
		$this->modelName = "Variable";
	}

	protected function repository()
	{
		return $this->repository;
	}

	protected function listByFilter($start, $length, array $filters = [])
	{
        $variables = $this->repository->findByCondition(
            VariableCondition::all(),
            null,
            VariableCondition::pagerBySequence($start, $length),
            VariableCondition::sortById()
        );
        $count = $this->repository->countByCondition(VariableCondition::all());
        return [$variables, $count];
	}

    public function create(CreateRequest $request)
    {
        $model = $request->model();
        // TODO platform & pattern unique

        $this->repository()->create($model);
        return $this->renderApi($model);
    }

    public function update(UpdateRequest $request, $id)
    {
        $model = $this->checkModelById($id);
        $model = $request->model($model);
        // TODO platform & pattern unique

        $this->repository()->update($model);
        return $this->renderApi($model);
    }

    public function delete($id)
    {
        $id = $this->checkId($id);
        $this->checkTdk($id);
        $this->repository()->delete($id);
        return $this->renderApi();
    }
}