<?php

namespace KiwiCore\Http\Controllers\Admin;

use KiwiCore\Http\Controllers\AdminController;
use Illuminate\Http\Request;

class HomeController extends AdminController
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return $this->render('home');
    }
}
