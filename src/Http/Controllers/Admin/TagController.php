<?php

namespace KiwiCore\Http\Controllers\Admin;


use KiwiCore\Condition\TagCondition;
use KiwiCore\Exceptions\ArgumentException;
use KiwiCore\Http\Controllers\AdminController;
use KiwiCore\Repository\TagRelationRepository;
use KiwiCore\Repository\TagRepository;
use KiwiCore\Http\Request\Admin\Tag\CreateRequest;
use KiwiCore\Http\Request\Admin\Tag\UpdateRequest;
use KiwiCore\Service\CheckById;

class TagController extends AdminController
{
	use TableTrait, CheckById;

	protected static $columns = [
		["name" => "id", "type" => "id", "title" => "ID", "data" => "id"],
		["name" => "__op", "type" => "op", "title" => "操作", "data" => "__op", "target" => [
			["name" => "update", "title" => "编辑"],
			["name" => "remove", "title" => "删除"],
		]],
		["name" => "name", "type" => "string", "title" => "英文名称", "data" => "name"],
		["name" => "title", "type" => "string", "title" => "中文名称", "data" => "title"],
	];

	protected static $filters = [
		["name" => "title"],
	];

	protected $tagRepository;

	protected $tagRelationRepository;

	public function __construct()
	{
		parent::__construct();
		$this->tagRepository = app(TagRepository::class);
		$this->tagRelationRepository = app(TagRelationRepository::class);
		$this->modelName = "Tag";
	}

	protected function repository()
	{
		return $this->tagRepository;
	}

	protected function listByFilter($start, $length, array $filters = [])
	{
		// title filter
        $title = $filters["title"];
        $filter = new \stdClass();
        if (!empty($title)) {
            $filter->title = $title;
        }

        $condition = TagCondition::byFilter($filter);
        $tags = $this->tagRepository->findByCondition(
            $condition,
            null,
            TagCondition::pagerBySequence($start, $length),
            TagCondition::sortById()
        );

        $count = $this->tagRepository->countByCondition($condition);
        return [$tags, $count];
	}

    public function create(CreateRequest $request)
    {
        $tag = $request->model();
        $this->checkNameUnique($tag->name);
        $this->repository()->create($tag);
        return $this->renderApi($tag);
    }

    public function update(UpdateRequest $request, $id)
    {
        $id = $this->checkId($id);
        $tag = $this->checkTag($id);
        $name = $tag->name;
        $tag = $request->model($tag);

        if ($tag->name !== $name) {
            $this->checkNameUnique($tag->name);
        }
        if ($tag->name !== $name) {
            $this->checkArticleAlreadyUse($name);
        }

        $this->repository()->update($tag);
        $this->relyChange($name, $tag->name);
        return $this->renderApi($tag);
    }

    public function delete($id)
    {
        $id = $this->checkId($id);
        $tag = $this->checkTag($id);
        $this->checkArticleAlreadyUse($tag->name);
        $this->repository()->delete($id);
        $this->tagRelationRepository->query()->where(['tag' => $tag->name])->delete();
        return $this->renderApi();
    }

    protected function checkNameUnique($name)
    {
        $condition = TagCondition::byName($name);
        if ($this->tagRepository->countByCondition($condition) !== 0) {
            throw new ArgumentException("name or title exist $name");
        }
    }

    protected function checkTitleUnique($title)
    {
        $condition = TagCondition::byTitle($title);
        if ($this->tagRepository->countByCondition($condition) !== 0) {
            throw new ArgumentException("name or title exist $title");
        }
    }

    protected function checkArticleAlreadyUse($name)
    {
        // TODO
    }

    public function search()
    {
        $title = request("q");
        $tags = $this->tagRepository->findByCondition(
            TagCondition::search($title)
        );
        return $this->renderApi($tags);
    }

    /**
     * 依赖变更
     * @param $tagNameOrigin
     * @param $tagName
     */
    public function relyChange($tagNameOrigin, $tagName){
        if ($tagNameOrigin === $tagName){
            return;
        }

        return $this->tagRelationRepository->query()->where(['tag' => $tagNameOrigin])->update(['tag' => $tagName]);
    }
}