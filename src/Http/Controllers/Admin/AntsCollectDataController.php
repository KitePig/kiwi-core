<?php

namespace KiwiCore\Http\Controllers\Admin;


use Illuminate\Http\Request;
use KiwiCore\Condition\AntsCollectDataCondition;
use KiwiCore\Model\AntsCollectData;
use KiwiCore\Repository\AntsCollectDataRepository;
use KiwiCore\Repository\AntsCollectTaskRepository;
use KiwiCore\Service\AntsCollectFormatter;

class AntsCollectDataController extends AntsBaseController
{
    protected $repository;

    protected $antsCollectTaskRepository;


    public function __construct()
    {
        parent::__construct();
        $this->repository = app(AntsCollectDataRepository::class);
        $this->antsCollectTaskRepository = app(AntsCollectTaskRepository::class);
    }

    public function data($id){
        $task = $this->antsCollectTaskRepository->findById($id);

        return $this->render('ants-task.data', compact('task'));
    }

    public function edit($id){
        $data = $this->repository->findById($id);

        return $this->render('ants-task.edit', compact('data'));
    }

    public function update($id, Request $request){
        $model = $this->repository->findById($id);
        $detail = $this->updateModel($model, $request);

        $task = $this->antsCollectTaskRepository->findById($model->task_id);
        $formatter = new AntsCollectFormatter($task, $detail);
        $formatter->importImage();
        $formatter->formatToDb();

        $this->repository->update($model);

        return $this->renderApi(AntsBaseController::returnAntsApi(AntsBaseController::TASK_SUCCESS, $model));
    }

    public function batchOperation($id, Request $request){

        $operation = $request->input('batchOperation');
        $batchIds = $request->input('batchIds');

        if ($operation == '100'){
            $this->repository->query()->whereIn('id', $batchIds)->delete();
        }

        foreach ($this->repository->findByIds($batchIds) as $data){
            $data->state = $operation;
            $data->update();
        }

        return $this->renderApi(AntsBaseController::returnAntsApi(AntsBaseController::TASK_SUCCESS));
    }

    /**
     * @param $id
     * @return array
     */
    public function paging($id){
        $page = request()->input('page', 1);
        $perPage = request()->input('perPage', 10);
        $state = request()->input('state', AntsCollectData::STATE_ALl);

        $query = $this->repository->query();
        $query = AntsCollectDataCondition::state($id, $state)->call($this, $query);
        $query = AntsCollectDataCondition::sortById()->call($this, $query);
        $data = $query->paginate($perPage, ['*'], 'page', $page);
        $data->appends(['perPage' => $perPage]);
        $data->appends(['state' => $state]);
        foreach ($data as &$item){
            if (isset($item->image)){
                $item->image = image($item->image);
            } else {
                $item->image = '';
            }
        }

        return $this->renderApi(AntsBaseController::returnAntsApi(AntsBaseController::TASK_SUCCESS, $data));
    }

}