<?php

namespace KiwiCore\Http\Controllers\Admin\Auth;


use KiwiCore\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PasswordController extends AdminController
{
	public function editView()
	{
		return $this->render("auth.passwords.update");
	}

	public function update(Request $request)
	{
		$this->validate($request, [
			"old_password" => "required|min:6",
			"password" => "required|confirmed|min:6",
		]);
		$oldPassword = request("old_password");
		$password = request("password");

		// check old password
		$user = Auth::guard()->user();
		$validate = Auth::guard()->validate([
			"id" => $user->getAuthIdentifier(),
			"password" => $oldPassword,
		]);
		if (!$validate) {
			throw new \Exception("old password is invalid");
		}

		// update new password
		$user->forceFill([
			"password" => bcrypt($password)
		])->save();

		return $this->renderApi();
	}
}