<?php

namespace KiwiCore\Http\Controllers\Admin;

use KiwiCore\Condition\ManualDataCondition;
use KiwiCore\Http\Controllers\AdminController;
use KiwiCore\Http\Request\Admin\ManualData\UpdateRequest;
use KiwiCore\Http\Request\Request;
use KiwiCore\Repository\ManualDataRepository;
use KiwiCore\Http\Request\Admin\ManualData\CreateRequest;
use KiwiCore\Service\CheckById;

class ManualDataController extends AdminController
{
    use TableTrait, CheckById;

    protected static $columns = [
        ["name" => "id", "type" => "id", "title" => "ID", "data" => "id"],
        ["name" => "__op", "type" => "op", "title" => "操作", "data" => "__op", "target" => [
            ["name" => "update", "title" => "编辑"],
            ["name" => "remove", "title" => "删除"],
        ]],
        ["name" => "type", "type" => "string", "title" => "类型", "data" => "type"],
        ["name" => "name", "type" => "string", "title" => "Name", "data" => "name"],
        ["name" => "title", "type" => "string", "title" => "名称", "data" => "title"],
        ["name" => "describe", "type" => "string", "title" => "描述", "data" => "describe"],
        ["name" => "groupCount", "type" => "string", "title" => "内容数量", "data" => "groupCount"],
    ];

    protected static $filters = [
    ];

    protected $repository;

    public function __construct(ManualDataRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
        $this->modelName = "ManualData";
    }

    protected function repository()
    {
        return $this->repository;
    }

    protected function listByFilter($start, $length, array $filters = [])
    {
        $manuleList = $this->repository->findByCondition(
            ManualDataCondition::all(),
            null,
            ManualDataCondition::pagerBySequence($start, $length),
            ManualDataCondition::sortById()
        );

        $manualDataMapsGroup = $this->repository()->manualDataMapsGroup();

        $manuleList->map(function ($list) use ($manualDataMapsGroup){
            $group = $manualDataMapsGroup->where('manual_id', $list->id)->first();
            $list->groupCount = $group?$group->manual_count:0;
        });

        return [$manuleList, $this->repository->countByCondition(ManualDataCondition::all())];
    }

    public function create(CreateRequest $request)
    {
        $model = $request->model();
        $this->repository()->create($model);
        return $this->renderApi($model);
    }

    public function update(UpdateRequest $request, $id)
    {
        $model = $this->checkModelById($id);
        $model = $request->model($model);

        $this->repository()->update($model);
        return $this->renderApi($model);
    }

    public function delete($id)
    {
        $id = $this->checkId($id);
        $this->repository()->delete($id);
        return $this->renderApi();
    }

    public function manualListByType($type, $id){
        $manualData = $this->repository()->manualDataByDataId($id);
        $manualDataIds = $manualData->pluck('manual_id');
        $manualList = $this->repository()->findByCondition(
            ManualDataCondition::manualColumns($type)
        );
        $manualList->map(function ($list) use ($manualDataIds) {
            $list->top = in_array($list->id, $manualDataIds->toArray())?1:2;
            return $list;
        });

        return $this->renderApi($manualList);
    }

    /**
     * 推送文章管理
     * @param Request $request
     * @return array
     */
    public function manualUpdate(Request $request){
        $manualId = $request->input('manual_id');
        $dataId = $request->input('data_id');
        $manualTop = $request->input('manual_top');
        if (empty($manualId) | empty($dataId) | !in_array($manualTop, ['1', '2'])){
            return $this->renderApi();
        }

        if ($manualTop === '1'){
            $this->repository()->manualDataMapsDelete($dataId); // 删
        } else {
            $this->repository()->manualDataMapsCreate($manualId, [$dataId]);// 增
        }
        return $this->renderApi();
    }
}