<?php

namespace KiwiCore\Http\Controllers\Admin;


use KiwiCore\Http\Controllers\AdminController;
use KiwiCore\Repository\AdminRepository;
use KiwiCore\Http\Request\Admin\Admin\CreateRequest;
use KiwiCore\Http\Request\Admin\Admin\UpdateRequest;

class AdminUserController extends AdminController
{
	use TableTrait;

	protected static $columns = [
		["name" => "id", "type" => "id", "title" => "ID", "data" => "id"],
		["name" => "__op", "type" => "op", "title" => "操作", "data" => "__op", "target" => [
			["name" => "update", "title" => "编辑"],
			["name" => "remove", "title" => "删除"],
		]],
		["name" => "name", "type" => "string", "title" => "用户名", "data" => "name"],
		["name" => "email", "type" => "string", "title" => "邮箱", "data" => "email"],
		["name" => "role", "type" => "string", "title" => "权限", "data" => "role"],
	];

	protected static $filters = [
		["name" => "name"],
	];

	/**
	 * @var AdminRepository
	 */
	protected $adminRepository;

	public function __construct()
	{
		parent::__construct();
		$this->adminRepository = app(AdminRepository::class);
		$this->modelName = "User";
	}

	protected function repository()
	{
		return $this->adminRepository;
	}

	protected function listByFilter($start, $length, array $filters = [])
	{
		$name = $filters["name"];

		if (empty($name)) {
			$admins = $this->adminRepository->find($start, $length);
			$count = $this->adminRepository->count();
		} else {
			$admin = $this->adminRepository->findByName($name);
			if (empty($admin)) {
				$admins = [];
				$count = 0;
			} else {
				$admins = [$admin];
				$count = 1;
			}
		}

		return [$admins, $count];
	}

    public function create(CreateRequest $request)
    {
        $model = $request->model();
        $model->password = bcrypt(config("kiwi.admin_pass"));
        $this->repository()->create($model);
        return $this->renderApi($model);
    }

    public function update(UpdateRequest $request, $id)
    {
        $model = $this->checkModelById($id);
        $model = $request->model($model);
        $this->repository()->update($model);
        return $this->renderApi($model);
    }

    public function delete($id)
    {
        $id = $this->checkId($id);
        $this->repository()->delete($id);
        return $this->renderApi();
    }
}