<?php
namespace KiwiCore\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use KiwiCore\Http\Controllers\AdminController;

class AntsBaseController extends AdminController
{
    const TASK_SUCCESS = 200;
    const TASK_ONGOING = 220;
    const TASK_FAILURE = 400;

    protected static $msg = [
        self::TASK_SUCCESS => ['ch' => '成功.', 'en' => 'Success.'],
        self::TASK_ONGOING => ['ch' => '进行中.', 'en' => 'Ongoing.'],
        self::TASK_FAILURE => ['ch' => '失败.', 'en' => 'Failure.'],
    ];

    public function updateModel(Model $model, Request $request){
        $allParams = $request->all();
        foreach ($allParams as $key => $val){
            if (isset($model->$key)){
                $model->$key = $val??'';
            }
        }
        return $model;
    }

    public static function returnAntsApi($code, $data = [], $message = '', $language = 'ch'){
        if ($message === '' && isset(self::$msg[$code])){
            $message = self::$msg[$code][$language];
        }
        return [
            'code' => $code,
            'data' => $data,
            'message' => $message
        ];
    }

}