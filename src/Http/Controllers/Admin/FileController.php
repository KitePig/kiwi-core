<?php

namespace KiwiCore\Http\Controllers\Admin;

use KiwiCore\Exceptions\AppException;
use KiwiCore\Exceptions\ArgumentException;
use KiwiCore\Http\Controllers\AdminController;
use KiwiCore\Service\ImageUploadServer;
use KiwiCore\Service\UpYun;
use Illuminate\Http\Request;

class FileController extends AdminController
{
	public function view()
	{
		return $this->render("file");
	}

	public function ckeditor(Request $request)
	{
		$result = $this->upload($request, "article");
		if ($result["status"] !== 0) {
			return $result["message"];
		}

		$param = [];
		$param["url"] = $result["data"]["url"];
		$param["CKEditorFuncNum"] = request("CKEditorFuncNum");
		$param["message"] = "";
		return $this->render("article.upload", $param);
	}

    /**
     * @param Request $request
     * @param string $type
     * @param string $name
     * @return array
     * @throws AppException
     * @throws ArgumentException
     */
	public function upload(Request $request, $type = "default", $name = "")
	{
        if ($request->files->count() === 0) {
            throw new ArgumentException("upload fail. upload file not found");
        }

        $uploadFile = $request->files->get($request->files->keys()[0]);
        if (!$uploadFile) {
            throw new ArgumentException("upload fail. upload file not found");
        }

        $filePath = $uploadFile->getRealPath();

        $url = app(ImageUploadServer::class)->upload($filePath, $type, $name);

        if (empty($url)) {
            throw new AppException("upload fail");
        }
        return $this->renderApi([
            "url" => $url,
        ]);
	}
}