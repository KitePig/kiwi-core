<?php

namespace KiwiCore\Http\Controllers\Admin;


use KiwiCore\Condition\ArticleChannelCondition;
use KiwiCore\Condition\ArticleCondition;
use KiwiCore\Condition\ArticleTagCondition;
use KiwiCore\Exceptions\NotFoundException;
use KiwiCore\Foundation\Environment;
use KiwiCore\Http\Controllers\Traits\ChannelNameChecker;
use KiwiCore\Http\Controllers\Traits\TagNameChecker;
use KiwiCore\Http\Controllers\AdminController;
use KiwiCore\Model\Article;
use KiwiCore\Model\Recommend;
use KiwiCore\Repository\ArticleRepository;
use KiwiCore\Repository\ChannelRelationRepository;
use KiwiCore\Repository\ChannelRepository;
use KiwiCore\Repository\RecommendRepository;
use KiwiCore\Repository\TagRelationRepository;
use KiwiCore\Repository\TagRepository;
use KiwiCore\Http\Request\Admin\Article\CreateRequest;
use KiwiCore\Http\Request\Admin\Article\UpdateRequest;
use KiwiCore\Service\ArticleFormatter;
use KiwiCore\Service\CheckById;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ArticleController extends AdminController
{
	use TableTrait, CheckById, ChannelNameChecker, TagNameChecker;

	protected static $columns = [
		["name" => "id", "type" => "id", "title" => "ID", "data" => "id"],
		["name" => "__op", "type" => "op", "title" => "操作", "data" => "__op", "target" => [
			["name" => "preview", "title" => "PC"],
			["name" => "previewm", "title" => "M"],
			["name" => "update", "title" => "Edit"],
			["name" => "remove", "title" => "Delete"],
			["name" => "publish", "title" => "Publish"],
			["name" => "manual", "title" => "Push"],
		]],
        ["name" => "image", "type" => "thumbnail", "title" => "图片", "data" => "image"],
		["name" => "title", "type" => "string", "title" => "标题", "data" => "title"],
//		["name" => "short_title", "type" => "string", "title" => "短标题", "data" => "short_title"],
		["name" => "channel", "type" => "string", "title" => "主頻道", "data" => "channel"],
		["name" => "channels", "type" => "string", "title" => "其他頻道", "data" => "channels"],
		["name" => "tag", "type" => "string", "title" => "标签", "data" => "tags"],
		["name" => "state", "type" => "enum", "title" => "状态", "data" => "state", "target" => [
			0 => "未发布",
			1 => "已发布",
			2 => "已删除",
		]],
		["name" => "published_at", "type" => "datetime", "title" => "发布时间", "data" => "published_at"],
		["name" => "updated_at", "type" => "datetime", "title" => "更新时间", "data" => "updated_at"],
		["name" => "content", "type" => "text", "title" => "内容", "visible" => false],
	];

	protected static $filters = [
		["name" => "title"],
//		["name" => "channel", "type" => "pointer"],
		["name" => "content"],
	];

	/**
	 * @var ArticleRepository
	 */
	protected $repository;

	/**
	 * @var ChannelRepository
	 */
	protected $channelRepository;

	/**
	 * @var TagRepository
	 */
	protected $tagRepository;

    /**
     * @var TagRelationRepository
     */
	protected $tagRelationRepository;
	protected $channelRelationRepository;
	
    /**
     * ArticleController constructor.
     * @param ArticleRepository $repository
     * @param TagRepository $tagRepository
     * @param TagRelationRepository $tagRelationRepository
     * @param ChannelRelationRepository $channelRelationRepository
     * @param ChannelRepository $channelRepository
     */
	public function __construct(
        ArticleRepository $repository,
        TagRepository $tagRepository,
        TagRelationRepository $tagRelationRepository,
        ChannelRelationRepository $channelRelationRepository,
        ChannelRepository $channelRepository)
	{
		parent::__construct();
		$this->repository = $repository;
        $this->tagRelationRepository = $tagRelationRepository;
        $this->channelRelationRepository = $channelRelationRepository;
		$this->channelRepository = $channelRepository;
		$this->tagRepository = $tagRepository;
		$this->modelName = "Article";
	}

    public function repository(){
        return $this->repository;
    }

    protected function listByFilter($start, $length, array $filters = [])
    {
        $title = $filters["title"];
        $content = $filters["content"];
        $channel = $filters["channel"];

        $filter = new \stdClass();
        if (!empty($title)) {
            $filter->title = $title;
        }
        if (!empty($content)) {
            $filter->content = $content;
        }
        if (!empty($channel)) {
//            $filter->channels = $this->channelRepository->findFamilyByName($channel);
        }

        $condition = ArticleCondition::byFilter($filter);
        $articles = $this->repository->findByCondition(
            $condition,
            null,
            ArticleCondition::pagerBySequence($start, $length),
            ArticleCondition::sortById()
        );
        $count = $this->repository->countByCondition($condition);

        $this->otherChannels($articles);
        $this->translateChannel($articles);
        $this->tags($articles);

        $opFilter = function ($op, $article) {
            if ($op["name"] === "remove") {
                return $article->state !== 2;
            }
            if ($op["name"] === "update") {
                return $article->state !== 2;
            }
            if ($op["name"] === "publish") {
                return $article->state === 0;
            }
            return true;
        };

        return [$articles, $count, $opFilter];
    }

    protected function createOpFilter()
    {
        return function ($op, $item, $name) {
            if ($name === "__op") {
                $name = $op["name"];
                if ($item->state === Article::STATE_DELETED){
                    if ($name === 'publish'){
                        return true;
                    }
                    return false;
                } else {
                    if ($name === 'publish'){
                        return false;
                    }
                }

//                if ($name === "preview") {
//                    return !in_array($item->state, [Article::STATE_UN_PUBLISH, Article::STATE_DELETED]);
//                }
//                if ($name === "previewm") {
//                    return !in_array($item->state, [Article::STATE_UN_PUBLISH, Article::STATE_DELETED]);
//                }
//                if ($name === "update") {
//                    return $item->state !== 2;
//                }
//                if ($name === "remove") {
//                    return $item->state === Article::STATE_PUBLISHED || $item->state === Article::STATE_UN_PUBLISH;
//                }
//                if ($name === "publish") {
//                    return $item->state === Article::STATE_DELETED || $item->state === Article::STATE_UN_PUBLISH;
//                }
//                if ($name === "push") {
//                    return !in_array($item->state, [Article::STATE_UN_PUBLISH, Article::STATE_DELETED]);
//                }
            }
            return true;
        };
    }

    public function create(CreateRequest $request)
    {
        $article = $request->model();
        $article->state = Article::STATE_PUBLISHED;
        $article->published_at = Carbon::now();

        // content
        $formatter = new ArticleFormatter($article);
        $formatter->importImage();
        $formatter->formatToDb();

        // article
        $id = $this->repository->create($article);

        // relation + 自动打标签
        $tags = $request->tags();
        $allTags = $this->tagRepository->all();
        $allTags->keyBy('title')->map(function ($val, $title) use ($article, &$tags){
            strpos($article->content, $title) !== false && array_push($tags, $val->name);
        });
        $this->tagRelationRepository->create($id, array_unique($tags));

        $channels = $request->channels();
        foreach ($channels as $channel) {
            $this->checkChannelExists($channel);
        }
        $channels = array_unique(array_merge($channels, [$article->channel]));
        $this->channelRelationRepository->create($id, $channels);

        return $this->renderApi($article);
    }

    public function update(UpdateRequest $request, $id)
    {
        $id = $this->checkId($id);
        $article = $this->checkArticle($id);

        $article = $request->model($article);

        // content
        $formatter = new ArticleFormatter($article);
        $formatter->importImage();
        $formatter->formatToDb();

        // article
        $this->repository->update($article);

        // relation
        $tags = $request->tags();
        $this->tagRelationRepository->delete($id);
        $allTags = $this->tagRepository->all();
        $allTags->keyBy('title')->map(function ($val, $title) use ($article, &$tags){
            strpos($article->content, $title) !== false && array_push($tags, $val->name);
        });
        $this->tagRelationRepository->create($id, array_unique($tags));

        $channels = $request->channels();
        foreach ($channels as $channel) {
            $this->checkChannelExists($channel);
        }
        $channels = array_unique(array_merge($channels, [$article->channel]));
        $this->channelRelationRepository->delete($id);
        $this->channelRelationRepository->create($id, $channels);

        return $this->renderApi($article);
    }

    public function publish($id)
    {
        $this->checkId($id);
        $article = $this->checkArticle($id);
        $article->state = Article::STATE_PUBLISHED;
        $article->published_at = Carbon::now();

        // content
        $formatter = new ArticleFormatter($article);
        $formatter->formatToDb();

        $this->repository->update($article);
        return $this->renderApi();
    }

    protected function infoAppend(Model &$article)
    {
        // 待封装
        $article->channels = $this->channelRelationRepository->findByCondition(
            ArticleChannelCondition::articleId($article->id)
        )->pluck('channel');
        $article->channels = collect($article->channels)->filter(function ($item) use ($article) {
            return $item !== $article->channel;
        })->values();
        $article->tags = $this->tagRelationRepository->findByCondition(
            ArticleTagCondition::articleId($article->id)
        )->pluck('tag')->toArray();
        $article = (new ArticleFormatter($article))->formatToWeb();
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function delete($id)
    {
        $id = $this->checkId($id);
        $article = $this->checkArticle($id);
        if (empty($article)){
            throw new NotFoundException("$this->modelName $id");
        }

        $this->repository->delete($id);

        return $this->renderApi();
    }

    /**
     * ************************************************************************************
     */

	public function editView()
	{
		return $this->render("article.edit", [
			"channels" => $this->channelRepository->all(),
			"tags" => $this->tagRepository->all(),
		]);
	}

	public function preview($id, $domain)
	{
		$this->checkId($id);
		$article = $this->repository->findById($id);
		if (empty($article)) {
			throw new NotFoundHttpException("Article $id");
		}

		$domain = intval($domain);
		$baseUrl = "";
		if ($domain === Environment::DOMAIN_PC) {
			$baseUrl = config("kiwi.domain.pc");
		}
		if ($domain === Environment::DOMAIN_MOBILE) {
			$baseUrl = config("kiwi.domain.mobile");
		}

		$linkFactory = app("linkFactory");
		$articleLink = $linkFactory->article($article->id);

		$scheme = config("kiwi.scheme.web");
        return redirect("$scheme://$baseUrl{$articleLink}");
	}



	public function getRecommend($id){
	    $recommend = app(RecommendRepository::class)->query()->select('mapping')->where('article_id', $id)->get();

        return $this->buildResult($recommend->pluck('mapping'));
    }

	public function recommend($id, Request $request)
	{
		$this->checkId($id);
        $recommend = $request->input('recommend_button_type', '');
        $recommend_cover = $request->input('recommend_cover', '');

        $article = $this->repository->findById($id);
        if (empty($article)) {
            throw new NotFoundHttpException("$this->modelName $id");
        }

        $recommendRepository = new Recommend();
        $recommendRepository->grouping = 'recommend'.$recommend;
        $recommendRepository->article_id = $id;
        $recommendRepository->cover = $recommend_cover;
        $recommendRepository->updated_at = now();
        $recommendRepository->created_at = now();
        $recommendRepository->save();

		return $this->buildResult();
	}

	/**
	 * @param Collection $articles
	 */
	protected function translateChannel($articles)
	{
		$channels = $this->channelRepository->all();
		foreach ($articles as &$article) {
			$channel = $channels->where("name", $article->channel)->first();
			if (!empty($channel)) {
				$article->channel = $channel->title;
			}
		}
	}

	/**
	 * @param Collection $articles
	 */
	protected function otherChannels($articles)
	{
        $articleIds = $articles->pluck("id");
        $relations = $this->channelRelationRepository->findByCondition(
            ArticleChannelCondition::articleIds($articleIds->toArray())
        );

        $channels = $this->channelRepository->all();
        foreach ($articles as &$article) {
            $article->channels = collect([]);
            foreach ($relations as $relation) {
                if ($relation->article_id === $article->id) {
                    $channel = $channels->where("name", $relation->channel)->first();
                    if (empty($channel)) {
                        continue;
                    }
                    if (!empty($channel->title)) {
                        $channel = $channel->title;
                    }
                    if ($relation->channel === $article->channel) {
                        continue;
                    }
                    $article->channels->push($channel);
                }
            }
            $article->channels = $article->channels->implode(",");
        }
	}

	private function tags(Collection $articles)
	{
        $articleIds = $articles->pluck("id");
        $relations = $this->tagRelationRepository->findByCondition(
            ArticleTagCondition::articleIds($articleIds->toArray())
        );

        $tags = $this->tagRepository->all();
        foreach ($articles as &$article) {
            $article->tags = collect([]);
            foreach ($relations as $relation) {
                if ($relation->article_id === $article->id) {
                    $tag = $tags->where("name", $relation->tag)->first();
                    if (empty($tag)) {
                        continue;
                    }
                    if (!empty($tag->title)) {
                        $tag = $tag->title;
                    }
                    if ($relation->tag === $article->tag) {
                        continue;
                    }
                    $article->tags->push($tag);
                }
            }
            $article->tags = $article->tags->implode(",");
        }
	}

    /**
     * @param Article $article
     */
	protected function setChannelRelation(Article $article)
	{
		$this->channelRepository->deleteRelation($article->id);

		$channels = array_merge($article->channels, [$article->channel]);
		$channels = array_unique($channels);
		$this->channelRepository->createRelation($article->id, $channels);
	}

	protected function setTagRelation(Article $article)
	{
		$this->tagRepository->deleteRelation($article->id);
		$tags = array_unique($article->tags);
		$this->tagRepository->createRelation($article->id, $tags);
	}
}