<?php

namespace KiwiCore\Http\Controllers\Traits;


use KiwiCore\Condition\ChannelCondition;
use KiwiCore\Model\Channel;
use KiwiCore\Repository\ChannelRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait ChannelNameChecker
{
    /**
     * @param string $name
     * @return Channel
     */
    protected function checkChannelExists($name)
    {
        $channelRepository = app(ChannelRepository::class);
        $channel = $channelRepository->findByCondition(ChannelCondition::channel($name));
        if (empty($channel)) {
            throw new NotFoundHttpException("Channel $name");
        }
        return $channel;
    }

    /**
     * @param string $name
     * @return bool
     */
    protected function checkChannelExistsNoException($name)
    {
        $channelRepository = app(ChannelRepository::class);
        if (empty($channelRepository->findByCondition(ChannelCondition::channel($name)))) {
            return false;
        }
        return true;
    }
}