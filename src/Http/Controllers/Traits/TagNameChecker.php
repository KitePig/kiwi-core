<?php

namespace KiwiCore\Http\Controllers\Traits;


use KiwiCore\Repository\TagRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait TagNameChecker
{
	protected function checkTagExists(array $tagNames)
	{
		$tagRepository = app(TagRepository::class);
		foreach ($tagNames as $tagName) {
			if (empty($tagRepository->findByName($tagName))) {
				throw new NotFoundHttpException("Tag $tagName");
			}
		}
	}

	protected function checkTagExistsNoException(array $tagNames)
	{
		$tagRepository = app(TagRepository::class);
		foreach ($tagNames as $tagName) {
			if (empty($tagRepository->findByName($tagName))) {
				return false;
			}
		}
		return true;
	}
}