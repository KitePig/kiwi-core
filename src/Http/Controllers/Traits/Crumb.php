<?php

namespace KiwiCore\Http\Controllers\Traits;


use KiwiCore\Repository\ChannelRepository;
use KiwiCore\Service\LinkFormat;
use Illuminate\Support\Collection;

trait Crumb
{
    /**
     * @param string $channel
     * @return Collection
     */
    protected function buildCrumb($channel)
    {
        $repository = app(ChannelRepository::class);
        $channels = $repository->findChainByName($channel);

        $link = app(LinkFormat::class);
        $linkFactory = app("linkFactory");

        return collect($channels)->map(function ($name) use ($repository, $link, $linkFactory) {
            $channel = $repository->findByName($name);

            $result = new \stdClass();
            $result->title = $channel->title;
            $result->link = $link->full($linkFactory->topic($channel->name));
            return $result;
        });
    }
}