<?php
namespace KiwiCore\Http\Controllers;

use Illuminate\Support\Facades\View;
use KiwiCore\Foundation\Environment;
use KiwiCore\Repository\FriendLinkRepository;
use KiwiCore\Service\TdkService;

class WebController extends Controller
{

    public $friendLinkRepository;

    public function __construct()
    {
        $this->friendLinkRepository = app(FriendLinkRepository::class);
    }

    protected function render($view, $urlPattern, $data = [], $mergeData = [])
    {
        $this->createTDK($data, $urlPattern);
        $this->createFriendLink($data, $urlPattern);

        if (View::exists($view)) {
            return view($view, $data, $mergeData);
        } else {
            return view("kiwi::$view", $data, $mergeData);
        }
    }

    /**
     * @param array $data
     * @param $urlPattern
     */
    protected function createTDK(array &$data, $urlPattern)
    {
        if ($urlPattern == '')
            return ;

        $tdkService = app(TdkService::class);
        if (isset($data["tdk"])) {
            $data["tdk"] = $tdkService->find($urlPattern, $data["tdk"]);
        } else {
            $data["tdk"] = $tdkService->find($urlPattern);
        }
    }

    /**
     * @param array $data
     * @param string $urlPattern
     */
    protected function createFriendLink(array &$data, $urlPattern)
    {
        // friend link 只和内容有关系，所以不再取别名
        $data["friendLinks"] = app(FriendLinkRepository::class)->findByPattern($urlPattern, Environment::$domain);
    }

}