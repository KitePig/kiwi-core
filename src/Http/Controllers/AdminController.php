<?php
namespace KiwiCore\Http\Controllers;


use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
class AdminController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(){
    }

    protected function render($view, $data = [])
    {
        $configMenu = config("menu")??config("kiwiMenu"); // 优先使用menu避免同步core文件覆盖新路由
        $mainMenu = [];
        foreach ($configMenu as $configHead) {
            $configHead["can"] = isset($configHead["can"]) ? $configHead["can"] : 'editor';
            if (Gate::allows($configHead["can"])) {
                $head = new \stdClass();
                $head->title = $configHead["title"];
                $head->sub = [];

                if (!empty($configHead["sub"])) {
                    foreach ($configHead["sub"] as $configItem) {
                        $configItem["can"] = isset($configItem["can"]) ?
                            $configItem["can"] :
                            'editor';
                        if (Gate::allows($configItem["can"])) {
                            $item = new \stdClass();
                            foreach ($configItem as $key => $value) {
                                $item->$key = $value;
                            }
                            $head->sub[] = $item;
                        }
                    }
                }
                $mainMenu[] = $head;
            }
        }
        $data["mainMenu"] = $mainMenu;

        $data["site"] = request()->input('site', ''); // 多站点使用同一后台

        if (View::exists($view)) {
            return view($view, $data);
        } else {
            return view("kiwi::$view", $data);
        }
    }

    protected function checkId($id)
    {
        $validator = Validator::make(["id" => $id], ["id" => "required|integer|min:1"]);
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
        return intval($id);
    }

    protected function renderApi($data = null)
    {
        if ($data === null) {
            return [
                "status" => 0
            ];
        }
        return [
            "status" => 0,
            "data" => $data
        ];
    }
}