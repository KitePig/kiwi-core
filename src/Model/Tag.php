<?php
namespace KiwiCore\Model;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = "tag";

    protected $guarded = [];
}