<?php
namespace KiwiCore\Model;

use Illuminate\Database\Eloquent\Model;

class AntsCollectData extends Model
{
    const STATE_NO_COMPLETE = 0;
    const STATE_COMPLETE = 1;
    const STATE_PUBLISHED = 2;
    const STATE_FAIL = 3;
    const STATE_ALl = 100;

    protected $table = "ants_collect_data";
    protected $guarded = [];
}