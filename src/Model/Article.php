<?php

namespace KiwiCore\Model;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	const STATE_UN_PUBLISH = 0;
	const STATE_PUBLISHED = 1;
	const STATE_DELETED = 2;

	protected $table = "article";
    protected $guarded = [];

    protected $dates = ['published_at'];
}