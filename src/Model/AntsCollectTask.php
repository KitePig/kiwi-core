<?php
namespace KiwiCore\Model;

use Illuminate\Database\Eloquent\Model;

class AntsCollectTask extends Model
{
    // 0=正常, 1=采集完成, 2=删除
    const STATE_NORMAL = 0;
    const STATE_COMPLETE = 1;
    const STATE_DELETE = 2;

    // 自动采集 1=启动, 2=关闭
    const AUTO_COLLECT_CUSTOM = 1;
    const AUTO_COLLECT_SHUTDOWN = 2;

    // 自动发布 1=独立, 2=全局, 3=关闭
    const AUTO_PUBLISHED_CUSTOM = 1;
    const AUTO_PUBLISHED_GLOBAL = 2;
    const AUTO_PUBLISHED_SHUTDOWN = 3;

    protected $table = "ants_collect_task";
    protected $guarded = [];
}