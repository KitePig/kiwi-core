<?php

namespace KiwiCore\Model;

class FriendLink extends BaseModel
{
	protected $table = 'friend_link';
	protected $diffSite = true;

	protected $columns = [
		"id",
		"link",
		"title",
		"position",
		"pattern",
		"domain",
		"created_at",
		"updated_at",
	];
}