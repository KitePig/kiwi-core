<?php

namespace KiwiCore\Model;

class ManualDataMaps extends BaseModel
{
    protected $table = 'manual_data_maps';
    protected $primaryKey = 'id';
    protected $guarded = [];
}