<?php

namespace KiwiCore\Model;

class ManualData extends BaseModel
{
    protected $table = 'manual_data';
    protected $primaryKey = 'id';
    protected $guarded = [];
}