<?php
namespace KiwiCore\Model;

use Illuminate\Database\Eloquent\Model;

class AntsCollectPublished extends Model
{
    protected $table = "ants_collect_published";
    protected $guarded = [];
}