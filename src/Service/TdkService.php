<?php
namespace KiwiCore\Service;

use KiwiCore\Model\Tdk;
use KiwiCore\Repository\TdkRepository;
use KiwiCore\Foundation\Environment;
use Illuminate\Database\Eloquent\Model;

class TdkService
{
    protected $tdkRepository;

    public function __construct()
    {
        $this->tdkRepository = app(TdkRepository::class);
    }

    public function find($urlPattern, array $data = []){

        $tdk = $this->tdkRepository->firstByPattern($urlPattern, Environment::$domain);
        if (!empty($tdk)){
            return $this->compile($tdk, $data);
        }

        $tdk = new \stdClass();
        $tdk->title = "";
        $tdk->description = "";
        $tdk->keywords = "";
        return $tdk;
    }

    public function compile(Tdk $tdk, array $data)
    {
        $values = [];
        foreach ($data as $key => $value) {
            $this->setValue($key, $value, $values);
        }

        if (isset($data['story']) && $tdk->pattern === '/g/\d+.html'){
            // 故事独立的特殊逻辑
            if ($data['story']->id <= 3621){
                $tdk->title = '{{story.title}}_{{story.channel}}_狸猫{{story.column}}';
                $tdk->description = '{{story.content}}';
                $tdk->keywords = '{{story.channel}}';
            } elseif ($data['story']->id <= 7578){
                $tdk->title = '{{story.title}}_{{story.channel}}_狸猫故事网';
                $tdk->description = '{{story.content}}';
                $tdk->keywords = '{{story.channel}}';
            }  elseif ($data['story']->id <= 9899){
                if (!strstr($values['story.title'], '的故事')){
                    $values['story.title'] = $values['story.title'].'的故事';
                }
                $tdk->title = '{{story.title}}_{{story.channel}}{{story.title}}完整版_狸猫故事网';
                $tdk->description = '【{{story.channel}}{{story.title}}】狸猫故事网推荐{{story.channel}}，{{story.title}}：{{story.content}}';
                $tdk->keywords = '{{story.title}},{{story.title}}完整版,{{story.channel}}{{story.title}}简短,{{story.title}}的主要内容';
            }
        }

        foreach ($values as $key => $value) {
            $tdk->title = str_replace("{{{$key}}}", $value, $tdk->title);
            $tdk->description = str_replace("{{{$key}}}", getTheUnlabeledString($value, 0, 240), $tdk->description);
            $tdk->keywords = str_replace("{{{$key}}}", $value, $tdk->keywords);
        }

        $title = "";
        if ($tdk->domain === Environment::DOMAIN_PC) {
            $title = config("kiwi.title.pc");
        }
        if ($tdk->domain === Environment::DOMAIN_MOBILE) {
            $title = config("kiwi.title.mobile");
        }

        $tdk->title = str_replace("{{site}}", $title, $tdk->title);
        $tdk->description = str_replace("{{site}}", $title, $tdk->description);
        $tdk->keywords = str_replace("{{site}}", $title, $tdk->keywords);

        return $tdk;
    }

    private function setValue($name, $object, array &$values)
    {
        if ($object instanceof Model) {
            $object = $object->toArray();
        }

        if (is_object($object)) {
            foreach (get_object_vars($object) as $key => $value) {
                $this->setValue("$name.$key", $value, $values);
            }
        } else if (is_array($object)) {
            foreach ($object as $key => $value) {
                $this->setValue("$name.$key", $value, $values);
            }
        } else {
            $values[$name] = $object;
        }
    }
}
