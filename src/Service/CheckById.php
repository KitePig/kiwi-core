<?php

namespace KiwiCore\Service;

use KiwiCore\Exceptions\NotFoundException;
use KiwiCore\Model\Article;
use KiwiCore\Repository\ArticleRepository;
use KiwiCore\Repository\ChannelRepository;
use KiwiCore\Repository\TagRepository;
use KiwiCore\Repository\TdkRepository;

trait CheckById
{
    protected function checkById($id){
        $result = $this->repository()->findById($id);
        if (empty($result)) {
            throw new NotFoundException("{$this->modelName} $id");
        }
        return $result;
    }
	/**
	 * @param $id
	 * @return Article
	 * @throws NotFoundException
	 */
    protected function checkArticle($id)
    {
        $article = app(ArticleRepository::class)->findById($id);
        if (empty($article)) {
            throw new NotFoundException("Article $id");
        }
        return $article;
    }

    /**
     * @param $id
     * @return mixed
     * @throws NotFoundException
     */
    protected function checkChannel($id)
    {
        $channel = app(ChannelRepository::class)->findById($id);
        if (empty($channel)) {
            throw new NotFoundException("Channel $id");
        }
        return $channel;
    }

    /**
     * @param $id
     * @return mixed
     * @throws NotFoundException
     */
	protected function checkTag($id)
	{
		$tag = app(TagRepository::class)->findById($id);
		if (empty($tag)) {
            throw new NotFoundException("Tag $id");
		}
		return $tag;
	}

    /**
     * @param $id
     * @return mixed
     * @throws NotFoundException
     */
	protected function checkTdk($id)
	{
		$tdk = app(TdkRepository::class)->findById($id);
		if (empty($tdk)) {
            throw new NotFoundException("TDK $id");
		}
		return $tdk;
	}
}