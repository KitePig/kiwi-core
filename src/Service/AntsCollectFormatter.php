<?php

namespace KiwiCore\Service;

use Closure;
use KiwiCore\Exceptions\ArgumentException;
use KiwiCore\Foundation\Environment;
use KiwiCore\Service\ImageUploadServer;
use KiwiCore\Service\LinkFormat;
use phpQuery;
use DOMElement;
use KiwiCore\Model\AntsCollectData;

class AntsCollectFormatter
{
    private $task;

    /**
	 * @var Data
	 */
	private $detail;

	/**
	 * @var Link
	 */
	private $link;


    /**
     * AntsCollectFormatter constructor.
     * @param $task
     * @param AntsCollectData $detail
     */
    public function __construct($task, AntsCollectData $detail)
	{
		$this->task = $task;
		$this->detail = $detail;
		$this->link = app(LinkFormat::class);
	}

	/**
	 * 下载第三方网站图片，上传到file or upYun，并修改链接，admin使用
	 * @return Data
	 */
	public function importImage()
	{
		$this->convertImage(/**
         * @param $src
         * @return mixed
         */
        function ($src) {

			// 目标网站图片相对链接
			if (!$this->link->isFull($src)) {
				throw new ArgumentException("class::AntsCollectFormatter image src cannot be relative link");
			}

			// 目标网站图片绝对链接
            if (!$this->link->isFullImage($src)) {
                $src = app(ImageUploadServer::class)->upload($src, 'article');
            }
			return $src;
		});
		return $this->detail;
	}

	/**
	 * @return Data
	 * @remark 图片修改为相对链接
	 */
	public function formatToDb()
	{
		$this->convertImage(
			function ($src) {
				return $this->link->relativeImage($src);
			}
		);
		if (!empty($this->detail->image)) {
			$this->detail->image = $this->link->relativeImage($this->detail->image);
		}

		return $this->detail;
	}

	/**
	 *
	 * @return string
	 * @remark 图片修改为完整链接，并调整大小
	 */
//	public function formatToWeb()
//	{
//		$this->convertImage(/**
//         * @param DOMElement $element
//         * @return DOMElement
//         */
//            function ($src) {
//			return $this->link->fullImage($src);
//		}, function (DOMElement $element) {
//			if (!Environment::isMobile()) {
//				return $element;
//			}
//			$style = $element->getAttribute("style");
//			if (empty($style)) {
//				return $element;
//			}
//
//			$result = [];
//			$sections = explode(";", $style);
//			foreach ($sections as $section) {
//				$keyValue = explode(":", $section);
//				if (strtolower(trim($keyValue[0])) === "width") {
//					$result[] = "width:100%";
//				} else if (strtolower(trim($keyValue[0])) === "height") {
//
//				} else {
//					$result[] = $section;
//				}
//			}
//			$element->setAttribute("style", implode(";", $result));
//			return $element;
//		});
//		return $this->detail;
//	}

    /**
     * @param Closure|null $srcConverter 修改属性值
     * @param Closure|null $elementConverter 修改属性元素
     */
	private function convertImage(
		Closure $srcConverter = null,
		Closure $elementConverter = null)
	{
		if (empty($this->detail->content) && empty($this->detail->image)) {
			return;
		}

		if (!empty($this->detail->image)){
            // 是否本地化图片并修改链接
            if ($this->task->image_download == 1 && !empty($srcConverter)) {
                $this->detail->image = $srcConverter($this->detail->image);
            }
        }


        $doc = phpQuery::newDocumentHTML($this->detail->content);
        foreach ($doc->find('img') as $image) {
            $src = $image->getAttribute($this->task->image_attribute);
            $src = app(AntsCollectService::class)->urlFormat($src, $this->task->address);
            if (empty($src))
                continue;

            // 是否本地化图片并修改链接
            if ($this->task->image_download == 1 && !empty($srcConverter)) {
                $src = $srcConverter($src);
            }
            $image->setAttribute('src', $src);
            if (!empty($elementConverter)) {
                $elementConverter($image);
            }
            if (empty($this->detail->image)) {
                $this->detail->image = $src;
            }
        }
        $this->detail->content = (string)$doc;
	}
}