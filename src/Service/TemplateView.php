<?php

namespace KiwiCore\Service;


use KiwiCore\Foundation\Environment;
use KiwiCore\Repository\TemplateRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Storage;

class TemplateView
{
	public function putFile($url, $domain, $content)
	{
		$fileName = $this->fileName($url, $domain);
		Storage::put("views/$fileName.blade.php", $content);
	}

	public function getFile($url, $domain)
	{
		$fileName = $this->fileName($url, $domain);
		if (Storage::exists("views/$fileName.blade.php")) {
			return Storage::get("views/$fileName.blade.php");
		}
		return "";
	}

	public function preview($url, $domain)
	{
		if ($domain == Environment::DOMAIN_PC) {
			$baseUrl = config("enle.domain.pc");
		} else {
			$baseUrl = config("enle.domain.mobile");
		}
		$scheme = config("enle.scheme.web");
		return redirect("$scheme://{$baseUrl}{$url}");
	}

	public function renderFile($data = [])
	{
		$request = app("request");
		$url = $request->path();
		$domain = Environment::$domain;

		if (empty($this->getFile($url, $domain))) {
			return "";
		}

		return $this->renderSrc($url, $domain, $data);
	}

	public function renderDatabase($data = [])
	{
		$request = app("request");
		$url = $request->path();
		$domain = Environment::$domain;

		$link = app(Link::class);
		$url = $link->withSlash($url);
		$repository = app(TemplateRepository::class);
		$template = $repository->findByUrlAndDomain($url, $domain);

		if (empty($template)) {
			return "";
		}

		$this->putFile($url, $domain, $template->content);
		return $this->renderSrc($url, $domain, $data);
	}

	private function renderSrc($url, $domain, $data)
	{
		app(Factory::class)->addLocation(storage_path("app/views"));
		$fileName = $this->fileName($url, $domain);
		return view($fileName, $data);
	}

	private function fileName($url, $domain)
	{
		$fileName = trim($url, "/");
		$fileName = str_replace("/", "-", $fileName);
		return "{$fileName}_{$domain}";
	}
}