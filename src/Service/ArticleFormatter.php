<?php

namespace KiwiCore\Service;


use Closure;
use KiwiCore\Exceptions\AppException;
use KiwiCore\Exceptions\ArgumentException;
use KiwiCore\Foundation\Environment;
use KiwiCore\Model\Article;
use DOMElement;
use phpQuery;
use Sunra\PhpSimple\HtmlDomParser;

class ArticleFormatter
{
	const SEGMENT_TEXT = 1;
	const SEGMENT_IMAGE = 2;
	const SEGMENT_VIDEO = 3;
	const SEGMENT_TITLE = 4;

	const IMAGE_ELEMENT = "data-eid";

	/**
	 * @var Article
	 */
	private $article;

	/**
	 * @var Link
	 */
	private $link;

	/**
	 * @var UpYun
	 */
	private $upYun;

	/**
	 * @var ImageRepository
	 */
	private $imageRepository;

	/**
	 * @var VideoRepository
	 */
	private $videoRepository;

    /**
     * ArticleFormatter constructor.
     * @param Article $article
     */
    public function __construct(Article $article)
	{
		$this->article = $article;
		$this->link = app(LinkFormat::class);
	}

	/**
	 * 下载第三方网站图片，上传到upYun，并修改链接，admin使用
	 * @return Article
	 */
	public function importImage()
	{
		$this->convertImage(function ($src) {

			// 第三方网站图片相对链接
			if (!$this->link->isFull($src)) {
				throw new ArgumentException("image src cannot be relative link");
			}

			// 第三方网站图片绝对链接
            if (!$this->link->isFullImage($src)) {
                try {
                    $src = app(ImageUploadServer::class)->upload($src, 'article');
                } catch (\Exception $e) {
                    throw new ArgumentException("image download fail $src");
                }
                if (empty($src)) {
                    throw new AppException("image upload fail $src");
                }
            }
			return $src;
		});
		return $this->article;
	}

	/**
	 * @return Article
	 * @remark 图片修改为相对链接
	 */
	public function formatToDb()
	{
//		$this->convertImage(
//			function ($src) {
//				return $this->link->relativeImage($src);
//			},
//			function (DOMElement &$element) {
//				$src = $element->getAttribute("src");
//				$id = $this->createImage($src);
//				$element->setAttribute(self::IMAGE_ELEMENT, $id);
//			}
//		);
		$this->convertImage(
			function ($src) {
				return $this->link->relativeImage($src);
			}
		);
		if (!empty($this->article->image)) {
			$this->article->image = $this->link->relativeImage($this->article->image);
		}
		return $this->article;
	}

//	private function createImage($src)
//	{
//		$image = $this->imageRepository->findByLink($src);
//		if (empty($image)) {
//			$fullSrc = $this->link->fullImage($src);
//			$info = $this->upYun->imageInfo($fullSrc);
//
//			$image = new ArticleImage();
//			$image->link = $src;
//			$image->height = $info->height;
//			$image->width = $info->width;
//			return $this->imageRepository->create($image);
//		} else {
//			return $image->id;
//		}
//	}

	/**
	 *
	 * @return string
	 * @remark 图片修改为完整链接，并调整大小
	 */
	public function formatToWeb()
	{
		$this->convertImage(function ($src) {
			return $this->link->fullImage($src);
		}, function (DOMElement $element) {
			if (!Environment::isMobile()) {
				return $element;
			}
			$style = $element->getAttribute("style");
			if (empty($style)) {
				return $element;
			}

			$result = [];
			$sections = explode(";", $style);
			foreach ($sections as $section) {
				$keyValue = explode(":", $section);
				if (strtolower(trim($keyValue[0])) === "width") {
					$result[] = "width:100%";
				} else if (strtolower(trim($keyValue[0])) === "height") {

				} else {
					$result[] = $section;
				}
			}
			$element->setAttribute("style", implode(";", $result));
			return $element;
		});
		return $this->article;
	}

	/**
	 *
	 * @return Article
	 * @remark 拆分为段
	 */
	public function formatToApp()
	{
		if ($this->article->category === Article::CATEGORY_VIDEO) {
			$this->article->segments = $this->videoSegments();
			return $this->article;
		}

		$segments = [];
		$dom = HtmlDomParser::str_get_html($this->article->content);
		$elements = $dom->find("p,h1,h2,h3,h4,h5,h6");
		foreach ($elements as $element) {
			if (!empty($segment = $this->segmentByElement($element))) {
				$segments[] = $segment;
			}
		}
		$this->article->segments = $segments;
		return $this->article;
	}

	private function videoSegments()
	{
		$videos = $this->videoRepository->findByArticleId($this->article->id);
		$segments = [];
		foreach ($videos as $video) {
			$image = $this->article->image;
			$html = config("sports.app.videoTemplate");
			$html = str_replace("{link}", $video->link, $html);
			$html = str_replace("{image}", $image, $html);
			$segments[] = (object)[
				"type" => self::SEGMENT_VIDEO,
				"w" => $video->width ?: 400,
				"h" => $video->height ?: 300,
				"content" => $html,
			];
		}
		return $segments;
	}

	private function segmentByElement(DOMElement $element)
	{
		switch (strtolower($element->tag)) {
			case "p":
				$img = $element->find("img", 0);
				if (empty($img)) {
					return $this->textSegment($element);
				} else {
					return $this->imageSegment($img);
				}
			case "h1":
			case "h2":
			case "h3":
			case "h4":
			case "h5":
			case "h6":
				return $this->titleSegment($element);
			default:
				return null;
		}
	}

	private function textSegment(DOMElement $element)
	{
		$text = $element->innertext();
		if ($text === "&nbsp;") {
			return null;
		}
		$textTemplate = config("sports.app.textTemplate");
		$html = str_replace("{text}", $text, $textTemplate);
		return (object)[
			"type" => self::SEGMENT_TEXT,
			"content" => $html,
		];
	}

	private function titleSegment(DOMElement $element)
	{
		$text = trim(strip_tags($element->text()), " \t\n\r\0\x0B");
		$text = str_replace("　", "", $text);    // 全角空格
		if ($text === "&nbsp;") {
			return null;
		}
		return (object)[
			"type" => self::SEGMENT_TITLE,
			"content" => $text,
		];
	}

	private function imageSegment(DOMElement $element)
	{
		$imageId = $element->getAttribute(self::IMAGE_ELEMENT);
		if (!empty($imageId)) {
			$image = $this->imageRepository->findById($imageId);
			if (!empty($image)) {
				return (object)[
					"type" => self::SEGMENT_IMAGE,
					"content" => $this->link->fullImage($image->link),
					"w" => $image->width,
					"h" => $image->height,
				];
			}
		}
		return null;
	}

    /**
     * @param Closure|null $srcConverter 修改属性值
     * @param Closure|null $elementConverter 修改属性元素
     */
	private function convertImage(
		Closure $srcConverter = null,
		Closure $elementConverter = null)
	{
		if (empty($this->article->content)) {
			return;
		}

        $doc = phpQuery::newDocumentHTML($this->article->content);
        foreach ($doc->find('img') as $image) {
            $src = $image->getAttribute("src");
            if (!empty($srcConverter)) {
                $src = $srcConverter($src);
            }
            $image->setAttribute('src', $src);
            if (!empty($elementConverter)) {
                $elementConverter($image);
            }
            if (empty($this->article->image)) {
                $this->article->image = $src;
            }
        }
        $this->article->content = (string)$doc;
	}
}