<?php

namespace KiwiCore\Service;


use Illuminate\Support\Str;
use KiwiCore\Foundation\Environment;
use KiwiCore\Repository\AliasLinkRepository;

class LinkFormat
{
    private static $imageScheme;
    private static $imageDomain;

    private static $webScheme;

    public static function boot()
    {
        if (empty(self::$imageScheme)) {
            self::$imageScheme = config("kiwi.scheme.image");
        }
        if (empty(self::$imageDomain)) {
            self::$imageDomain = config("kiwi.domain.image");
        }
        if (empty(self::$webScheme)) {
            self::$webScheme = config("kiwi.scheme.web");
        }
    }

    public function isFull($value)
	{
		return Str::startsWith($value, "//") ||
            Str::startsWith($value, "http://") ||
            Str::startsWith($value, "https://");
	}

	/**
	 * @param string $value
	 * @return string
	 */
	public function full($value)
	{
		// admin not convert to full
		if (Environment::isAdmin()) {
			return $value;
		}

		if (empty($value)) {
			return "";
		}
		// global link
		if (Str::startsWith($value, "//")) {
			return self::$webScheme.":$value";
		}
		// local link
		if (Str::startsWith($value, "/")) {
			$value = $this->alias($this->withSlash($value));
			return $this->baseUrl() . $value;
		}
		// already whole link or other, eg: "#overview"
		return $value;
	}

	/**
	 * @param string $value
	 * @return string
	 */
	public function relative($value)
	{
		// admin not convert to relative
		if (Environment::isAdmin()) {
			return $value;
		}

		if (empty($value)) {
			return $value;
		}
		$baseUrl = $this->baseUrl();
		if (Str::startsWith($value, $baseUrl)) {
			return str_replace($baseUrl, "", $value);
		}
		return $value;
	}

	/**
	 * @param string $value
	 * @return bool
	 */
	public function isFullImage($value)
	{
		if (empty($value)) {
			return false;
		}

		return Str::startsWith($value, self::$imageScheme . "://" . self::$imageDomain) ||
            Str::startsWith($value, "//".self::$imageDomain);
	}

	/**
	 * @param string $value
	 * @return string
	 */
	public function fullImage($value)
	{
	    if ($this->isFullImage($value)){
	        return $value;
        }

		if (empty($value)) {
			return "";
		}

		// css class
		if (Str::startsWith($value, ":")) {
			return substr($value, 1);
		}
		// placeholder
		if ($value === "#") {
			return $value;
		}

		return self::$imageScheme."://".self::$imageDomain.'/'.ltrim($value, '/');
	}

	/**
	 * @param string $value
	 * @return string
	 */
	public function relativeImage($value)
	{
		if (empty($value)) {
			return "";
		}
		if ($value === "#") {
			return $value;
		}

		if (Str::startsWith($value, self::$imageScheme . "://" . self::$imageDomain)) {
			return str_replace(self::$imageScheme . "://" . self::$imageDomain, "", $value);
		}
		if (Str::startsWith($value, "//" . self::$imageDomain)) {
			return str_replace("//" . self::$imageDomain, "", $value);
		}
		return "$value";
	}

	/**
	 * @param string $value
	 * @return string
	 */
	public function withSlash($value)
	{
//	    // 非.html 强制 / 结尾
//		if (!Str::endsWith($value, ".html") && !Str::endsWith($value, "/")) {
//			$value = "$value/";
//		}
		if (!Str::startsWith($value, "/")) {
			$value = "/$value";
		}
		return $value;
	}

	private function baseUrl()
	{
		return request()->getSchemeAndHttpHost();
	}

	private function alias($value)
	{
		return app(AliasLinkRepository::class)->globalAlias($value);
	}
}