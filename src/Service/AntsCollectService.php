<?php

namespace KiwiCore\Service;

use Illuminate\Support\Str;
use KiwiCore\Condition\AntsCollectDataCondition;
use KiwiCore\Http\Controllers\Admin\AntsBaseController;
use KiwiCore\Model\AntsCollectData;
use KiwiCore\Model\AntsCollectTask;
use KiwiCore\Repository\AntsCollectDataRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use KiwiCore\Model\Article;
use KiwiCore\Repository\TagRelationRepository;
use KiwiCore\Repository\TagRepository;
use QL\QueryList;

class AntsCollectService
{

    protected $repository;
    protected $antsCollectDataRepository;

    public function __construct()
    {
        $this->antsCollectDataRepository = app(AntsCollectDataRepository::class);
    }

    public function debug($address, $range, $rules, $remove_head = 1){

        $lists = $this->_QueryList($address, $remove_head)
            ->range($range)
            ->encoding('UTF-8')
            ->rules( $this->rulesFormat($rules) )
            ->query(function($list) {
                return $list;
            })
            ->getData();

        return AntsBaseController::returnAntsApi(AntsBaseController::TASK_SUCCESS, [$lists],
            'Success.'
        );
    }

    public function startListJob(AntsCollectTask $task){

        $lists = $this->_QueryList($task['address'], $task['remove_head'])
            ->range($task['list_range'])
            ->encoding('UTF-8')
            ->rules( $this->rulesFormat($task['list_rules']) )
            ->query(function($list) use ($task) {
                if (!empty($list)){
                    $list['link'] = $this->urlFormat($list['link'], $task->address);
                    return $list;
                }
            })
            ->getData();

        // 去重
        $heavy = $this->antsCollectDataRepository->findByCondition(
            AntsCollectDataCondition::sourceLinks($lists->pluck('link')->toArray())
        )->pluck('source_link')->toArray();

        $lists->map(function ($link) use ($task, $heavy){
            if (!in_array($link['link'], $heavy)){
                $model = new AntsCollectData();
                $model->task_id = $task->id;
                $model->source_link = $link['link'];
                $model->image = isset($link['image']) ? $this->urlFormat($link['image'], $task->address) : '';
                $this->antsCollectDataRepository->create($model);
            }
        });

        return AntsBaseController::returnAntsApi(AntsBaseController::TASK_SUCCESS, [],
            '本次采集数据'.$lists->count().'条, 其中去重'.count($heavy).'条, 列表数据采集结束..'
        );
    }

    public function startPagingJob(AntsCollectTask $task, $paging){
        $paging = explode('-',$paging);
        $pages = count($paging) === 2 ? range($paging[0] ,$paging[1]) : $paging;

        $lists = collect();
        foreach ($pages as $page){
            $items = $this->_QueryList(str_replace('{page}', $page, $task['paging_address']), $task['remove_head'])
                ->range($task['list_range'])
                ->encoding('UTF-8')
                ->rules( $this->rulesFormat($task['list_rules']) )
                ->query(function($list) use ($task) {
                    if (!empty($list)){
                        $list['link'] = $this->urlFormat($list['link'], $task->address);
                        return $list;
                    }
                })
                ->getData();

            $lists = $lists->merge($items);
        }

        // 去重
        $heavy = $this->antsCollectDataRepository->findByCondition(
            AntsCollectDataCondition::sourceLinks($lists->pluck('link')->toArray())
        )->pluck('source_link')->toArray();

        $lists->map(function ($link) use ($task, $heavy){
            if (!in_array($link['link'], $heavy)){
                $model = new AntsCollectData();
                $model->task_id = $task->id;
                $model->source_link = $link['link'];
                $model->image = isset($link['image']) ? $this->urlFormat($link['image'], $task->address) : '';
                $this->antsCollectDataRepository->create($model);
            }
        });

        return AntsBaseController::returnAntsApi(AntsBaseController::TASK_SUCCESS, [],
            '本次采集数据'.$lists->count().'条, 其中去重'.count($heavy).'条, 分页列表数据采集结束..'
        );
    }

    public function startJobDetails(AntsCollectTask $task, $stepLength = 10){

        $details = $this->antsCollectDataRepository->findByCondition(
            AntsCollectDataCondition::state($task->id, AntsCollectData::STATE_NO_COMPLETE),
            null,
            null,
            AntsCollectDataCondition::sortByIdAsc()
        );

        if ($details->isEmpty()){
            return AntsBaseController::returnAntsApi(
                AntsBaseController::TASK_SUCCESS, [], 'Collect complete.'
            );
        }

        $details->slice(0, $stepLength)->map(function($detail) use ($task) {
            try{
                if ( $task->remove_head == 1 ){
                    $data = QueryList::get($detail->source_link)
                        ->range($task->details_range)
                        ->encoding('UTF-8')
                        ->rules( $this->rulesFormat($task->details_rules) )->queryData();
                } else {
                    $data = QueryList::get($detail->source_link)->removeHead()
                        ->range($task->details_range)
                        ->encoding('UTF-8')
                        ->rules( $this->rulesFormat($task->details_rules) )->queryData();
                }
                if (empty($data)){
                    throw new \Exception('class::AntsCollectService->startJobDetails() Get Content Empty! Link: '.$detail->source_link);
                }
                $data = current($data);

                // 内容分页
                if (isset($data['content_paging']) && !empty($data['content_paging']) && $data['content_paging'] != '#'){
                    $this->detailsPaging($task, $data, $detail->source_link);
                }

                $detail->state = AntsCollectData::STATE_COMPLETE;
                $detail->title = $data['title'];
                $detail->content = $data['content'];
                $detail->message = 'Success.';
                $this->imageDownload($task, $detail);
                // 先下载,再替换。避免图片url被破坏
                $detail->title = getReplaceContent($detail->title, $task->keywords_replace);
                $detail->content = getReplaceContent($detail->content, $task->keywords_replace);
                $this->antsCollectDataRepository->update($detail);
            } catch (\Exception $e) {
                $detail->state = AntsCollectData::STATE_FAIL;
                $detail->content = '';
                $detail->message = mb_substr($e->getMessage(), 0, 255, 'utf-8');
                $this->antsCollectDataRepository->update($detail);
            }
        });

        return AntsBaseController::returnAntsApi(
            AntsBaseController::TASK_ONGOING, [], '本次处理' . $stepLength . '条数据, 剩余' . ($details->count() - $stepLength) . '条数据待采集. '
        );
    }

    /**
     * 内容分页采集
     * @param $task
     * @param $data
     * @param string $parentUrl //三级以上的相对链接、需要父url
     * @return mixed
     * @throws \Exception
     */
    public function detailsPaging($task, &$data, $parentUrl = ''){
        $data['content_paging'] = $this->urlFormat($data['content_paging'], $parentUrl);
        if (empty($data['content_paging']) || $data['content_paging'] === $parentUrl){
            return $data;
        }

        $detailsUrl = $data['content_paging'];
        if ( $task->remove_head == 1 ){
            $dataPaging = QueryList::get($detailsUrl)
                ->range($task->details_range)
                ->encoding('UTF-8')
                ->rules( $this->rulesFormat($task->details_rules) )->queryData();
        } else {
            $dataPaging = QueryList::get($detailsUrl)->removeHead()
                ->range($task->details_range)
                ->encoding('UTF-8')
                ->rules( $this->rulesFormat($task->details_rules) )->queryData();
        }
        $dataPaging = current($dataPaging);

        $data['content'] = $data['content'].$dataPaging['content'];
        $data['content_paging'] = $dataPaging['content_paging'];

        // 内容分页
        if (isset($data['content_paging']) && !empty($data['content_paging'])){
            return $this->detailsPaging($task, $data, $detailsUrl);
        }

        return $data;
    }

    /**
     * @param $taskIds
     * @param $rules 发布规则
     */
    public function autoPublishedData(array $taskIds, $rules){
        Log::info('Start.');
        Log::info('#######');
        Log::info('### Info: ', ['taskIds' => $taskIds, 'rules' => $rules]);
        Log::info('#######');
        foreach ($rules as $rule) {
            if (!isset($rule->type)){
                continue;
            }
            $rule->published_type = $rule->published_type??'all';
            $rule->published_array = $rule->published_array??[];

            Log::info('Rule-Type: ' . $rule->type);

            switch ($rule->type) {
                case 'article':
                    app(ArticleService::class)->articlePublished($taskIds, $rule);
                    break;
                case 'story':
                    app(\App\Service\StoryService::class)->storyPublished($taskIds, $rule);
                    break;
                default:
                    break;
            }
        }
        Log::info('Stop.');
    }

    public function urlFormat($src, $domain){
        if (empty($src) || $src === '#'){
            return '';
        }

        if (app(LinkFormat::class)->isFull($src)){
            return $src;
        }

        if (!Str::startsWith($src, '/')){
            // 相对路径
            return rtrim(substr($domain, 0, strrpos($domain,'/')), '/').'/'.ltrim($src, '/');
        }

        if (Str::startsWith($src, '/')){
            $domain = parse_url($domain);
            return $domain['scheme'].'://'.$domain['host'].'/'.ltrim($src, '/');
        }

        return '';
    }

    public function rulesFormat($rules){
        $result = array();
        foreach (json_decode($rules) as $key => $value){
            $result[$key] = $value;
        }
        return $result;
    }

    protected function _QueryList($url, $remove_head){
        if ( $remove_head == 1 ){
            return QueryList::get($url);
        }
        return QueryList::get($url)->removeHead();
    }

    /**
     * @param $task
     * @param $detail
     * @return mixed
     */
    protected function imageDownload($task, $detail)
    {
        $formatter = new AntsCollectFormatter($task, $detail);
        $formatter->importImage();
        $formatter->formatToDb();

        return $detail;
    }
}
