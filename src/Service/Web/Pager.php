<?php

namespace KiwiCore\Service\Web;


use KiwiCore\Service\LinkFormat;

class Pager
{
	public function split($word)
	{
		$sections = explode("_", $word);
		if (count($sections) === 1) {
			return [$word, 1];
		}

		$count = count($sections);
		$cursor = $sections[$count - 1];
		if (!is_numeric($cursor)) {
			return [$word, 1];
		}

		$word = implode("_", array_slice($sections, 0, $count - 1));
		$cursor = intval($sections[$count - 1]);
		if ($cursor < 1) {
			$cursor = 1;
		}
		return [$word, $cursor];
	}

	/**
	 * @param int $cursor
	 * @param int $pageSize
	 * @param int $itemCount
	 * @param string $baseUrl
	 * @return \stdClass
	 */
	public function create($cursor, $pageSize, $itemCount, $baseUrl)
	{
		$count = intval($itemCount / $pageSize);
		if ($itemCount % $pageSize !== 0) {
			++$count;
		}

		$link = app(LinkFormat::class)->full($baseUrl);
		$link = trim($link, "/");

		$pager = new \stdClass();
		$pager->cursor = $cursor;
		$pager->count = $count;
		$pager->baseUrl = $link;
		return $pager;
	}
}