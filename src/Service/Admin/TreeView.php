<?php

namespace KiwiCore\Service\Admin;


use Illuminate\Support\Collection;

class TreeView
{
    /**
     * @var array
     */
    private $items = [];

    /**
     * @var \stdClass
     */
    public $root;

    /**
     * item properties: id, name, title, parent
     * @param array|Collection $items
     * @return mixed
     */
    public function build($items)
    {
        $this->items = $items;

        $this->root = new \stdClass();
        $this->root->name = "";
        $this->root->title = "";
        $this->root->children = [];

        foreach ($this->items as $item) {
            $this->createNode($item);
        }

        $this->formatNode($this->root);

        return $this->root->children;
    }

    /**
     * 格式化成前端识别的格式
     * @param \stdClass $node
     */
    private function formatNode($node)
    {
        $node->name = $node->title;
        unset($node->title);
        foreach ($node->children as $child) {
            $this->formatNode($child);
        }
    }

    /**
     * @param \stdClass $item
     * @return \stdClass
     */
    private function createNode($item)
    {
        $node = $this->findNodeByName($item->name, $this->root);
        if (!empty($node)) {
            // 已经创建过了
            return $node;
        }

        $parentNode = $this->findNodeByName($item->parent, $this->root);
        if (empty($parentNode)) {
            $parentItem = $this->findItemByName($item->parent);
            if (empty($parentItem)) {
                // 无效的parent名称就给root
                $parentNode = $this->root;
            } else {
                $parentNode = $this->createNode($parentItem);
            }
        }

        $node = $this->newNode($item);
        $parentNode->children[] = $node;
        return $node;
    }

    /**
     * @param string $name
     * @return \stdClass|null
     */
    private function findItemByName($name)
    {
        foreach ($this->items as $item) {
            if ($item->name === $name) {
                return $item;
            }
        }
        return null;
    }

    /**
     * @param string $name
     * @param \stdClass $parentNode
     * @return \stdClass | null
     */
    private function findNodeByName($name, $parentNode)
    {
        if ($parentNode->name === $name) {
            return $parentNode;
        }
        foreach ($parentNode->children as $node) {
            if (!empty($result = $this->findNodeByName($name, $node))) {
                return $result;
            }
        }
        return null;
    }

    /**
     * @param $item
     * @return \stdClass
     */
    private function newNode($item)
    {
        $node = new \stdClass();
        $node->_id = $item->id;
        $node->id = $item->name;
        $node->name = $item->name;
        $node->title = "{$item->title} | {$item->name}";
        $node->children = [];
        return $node;
    }
}