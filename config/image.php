<?php

return [
    #file需要手动创建软连接  ln -s ../../storage/app/public/uploads public/pc/uploads ln -s ../../storage/app/public/uploads public/m/uploads
    'driver' => env('IMAGE_DRIVER', 'upYun'),

    'store' => [
        'file' => [
            'basePath' => env('IMAGE_BASE_PATH', storage_path("app/public")),
        ],

        'upYun' => [
            'bucket' => env("IMAGE_UP_YUN_BUCKET", ""),
            'operator' => env("IMAGE_UP_YUN_OPERATOR", ""),
            'password' => env("IMAGE_UP_YUN_PASSWORD", ""),
            'apiKey' => env("IMAGE_UP_YUN_API_KEY", ""),
        ],
    ],
];