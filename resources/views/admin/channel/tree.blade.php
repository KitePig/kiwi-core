@push("style")
<link rel="stylesheet" href="{{cdn("/jqtree/1.4.0/jqtree.css")}}">
@endpush

@prepend("script")
<script src="{{cdn("/jqtree/1.4.0/tree.jquery.min.js")}}"></script>
<script type="text/html" id="template_channel">
    <div class="box-body tree-panel"></div>
</script>
<script type="text/html" id="template_channels">
    <div class="box-body">
        <div class="tree-panel"></div>
        <a class="btn btn-primary btn-sm btn-flat submit_fab tree-select">确认</a>
    </div>
</script>
<script>
    // function(selectValue)
    var create_channel_selector = function (selected, callback) {
        var d = dialog({
            title: "选择频道",
            content: $("#template_channel").html()
        });
        d.showModal();

        var tree = $(".tree-panel");

        tree.tree("destroy");
        tools.get("/api/channels-tree", null, function (data) {
            tree.tree({
                data: data,
                autoOpen: false,
                dragAndDrop: false,
                selectable: true
            });
            tree.bind(
                'tree.dblclick',
                function (event) {
                    d.remove();
                    callback && callback(event.node.id);
                }
            );
            if (selected) {
                var node = tree.tree("getNodeById", selected);
                tree.tree('selectNode', node);
            }
        });
    };

    // function(selectValue)
    var create_channels_selector = function (selected, callback) {
        var d = dialog({
            title: "选择频道",
            content: $("#template_channels").html()
        });
        d.showModal();

        var tree = $(".tree-panel");

        tree.tree("destroy");
        tools.get("/api/channels-tree", null, function (data) {
            tree.tree({
                data: data,
                autoOpen: false,
                dragAndDrop: false,
                selectable: true
            });
            tree.on(
                'tree.click',
                function (event) {
                    // Disable single selection
                    event.preventDefault();

                    var selected_node = event.node;

                    if (selected_node.id === undefined) {
                        console.log('The multiple selection functions require that nodes have an id');
                    }

                    if (tree.tree('isNodeSelected', selected_node)) {
                        tree.tree('removeFromSelection', selected_node);
                    } else {
                        tree.tree('addToSelection', selected_node);
                    }
                }
            );
            if (selected) {
                selected.forEach(function (id) {
                    if (!id) {
                        return;
                    }
                    var node = tree.tree("getNodeById", id);
                    if (node) {
                        tree.tree('addToSelection', node);
                        if (node.parent) {
                            tree.tree("openNode", node.parent);
                        }
                    }
                });
            }

            $(".tree-select").on("click", function () {
                var ids = tree.tree('getSelectedNodes').map(function (node) {
                    return node.id;
                });
                d.remove();
                callback && callback(ids);
            });
        });
    }
</script>
@endpush