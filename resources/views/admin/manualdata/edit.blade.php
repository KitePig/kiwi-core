@extends("kiwi::common.edit")

@section("form")
    <div class="input-group text_title">
        <label for="type">类型</label>
        <input id="type" name="type" value="Article" type="text" class="form-control widthinu" required>
    </div>
    <div class="input-group text_title">
        <label for="name">Name</label>
        <input id="name" name="name" value="manual_?" type="text" class="form-control widthinu" required>
    </div>
    <div class="input-group text_title">
        <label for="title">名称</label>
        <input id="title" name="title" type="text" class="form-control widthinu" required>
    </div>
    <div class="input-group text_title">
        <label for="describe">描述</label>
        <input id="describe" name="describe" type="text" class="form-control widthinu">
    </div>
@endsection

@prepend("script")
<script>
    module.editor.setModel = function (model) {
        $("#type").val(model.type);
        $("#name").val(model.name);
        $("#title").val(model.title);
        $("#describe").val(model.describe);
    }
</script>
@endprepend