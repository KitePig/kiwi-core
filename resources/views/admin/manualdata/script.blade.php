@prepend("script")
<script>
    function manualList(id){
        tools.get("/api/manual/{{$model}}/list/"+id, null, function (manual_list) {
            var content_html = '';
            for(let list in manual_list){
                content_html += `
                    <button type="button"
                    manual-id="${manual_list[list].id}"
                    data-id="${id}"
                    manual-top="${manual_list[list].top}"
                    class="manual_button btn ${ifFunction(manual_list[list].top, 'btn-success', 'btn-info', 1)} margin"
                    value="${manual_list[list].name}">${manual_list[list].title}
                    (<i class="fa ${ifFunction(manual_list[list].top, 'fa-check-square-o', 'fa-times', 1)}"></i>)
                    </button>`;
            }

            var self = this;
            var root_d = dialog({
                title: '内容推送管理',
                content: content_html,
                cancelValue: '返回',
                cancel: function () {
                    root_d.remove();
                }
            });
            root_d.showModal();

            $(".manual_button").on('click', function () {
                var manual_id = $(this).attr('manual-id');
                var data_id = $(this).attr('data-id');
                var manual_top = $(this).attr('manual-top');

                var d = dialog({
                    title: '取消推送',
                    content: function () {
                        return manual_top === '1' ? '当前数据属于已推送状态, 确认将取消此数据的推送.' : '确认推送?';
                    },
                    okValue: '确认',
                    ok: function() {
                        tools.post('/api/manual/data/update', {
                            'manual_id': manual_id,
                            'data_id': data_id,
                            'manual_top': manual_top,
                        }, function () {
                            root_d.remove();
                            manualList(id);
                        });
                        d.remove();
                    },
                    cancelValue: '返回',
                    cancel: function () {
                        d.remove();
                    }
                });
                d.showModal();
            });

        });
    }
    function ifFunction(val, ifTrue, ifFalse, ifVar = false){
        if (ifVar) {
            return val === ifVar ? ifTrue : ifFalse;
        }
        return val === true ? ifTrue : ifFalse;
    }
</script>
@endprepend