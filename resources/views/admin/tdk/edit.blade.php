@extends("kiwi::common.edit")

@section("form")
    <div class="input-group text_title">
        <label for="pattern">URL</label>
        <input id="pattern" name="pattern" type="text" class="form-control widthinu" required>
    </div>
    <div class="input-group text_title">
        <label for="domain">站点</label>
        <select id="domain" name="domain" class="form-control widthinu" required>
            <option value="1">PC</option>
            <option value="2">移动</option>
        </select>
    </div>
    <div class="input-group text_title">
        <label for="title">Title</label>
        <input id="title" name="title" type="text" class="form-control widthinu" required>
    </div>
    <div class="input-group text_title">
        <label for="keywords">Keywords</label>
        <input id="keywords" name="keywords" type="text" class="form-control widthinu">
    </div>
    <div class="input-group text_title">
        <label for="description">Description</label>
        <textarea id="description" name="description" class="form-control widthinu" rows="5"></textarea>
    </div>
@endsection

@prepend("script")
<script>
    module.editor.setModel = function (model) {
        $("#title").val(model.title);
        $("#description").val(model.description);
        $("#keywords").val(model.keywords);
        $("#pattern").val(model.pattern);
        $("#domain").val(model.domain);
    }
</script>
@endprepend