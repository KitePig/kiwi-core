<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-body">
                @if(!isset($nocreate))
                    <a class="btn-create btn btn-primary btn-sm btn-flat" data-index="{{$index}}">添加</a>
                @endif
                <span class="filter" data-index="{{$index}}"></span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-body">
                <table class="data-table table table-bordered table-striped nowrap table-condensed"
                       data-model="{{$model}}" data-index="{{$index}}">
                </table>
            </div>
        </div>
    </div>
</div>