@extends("kiwi::common.edit")

@section("form")
    <div class="form-group">
        <label for="alias" class="control-label col-sm-4">别名</label>
        <div class="col-sm-8">
            <input id="alias" name="alias" type="text" class="form-control" required>
        </div>
    </div>
    <div class="form-group">
        <label for="link" class="control-label col-sm-4">内部链接</label>
        <div class="col-sm-8">
            <input id="link" name="link" type="text" class="form-control" required>
        </div>
    </div>
    <div class="form-group">
        <label for="global" class="control-label col-sm-4">全局替换</label>
        <div class="col-sm-8">
            <select id="global" name="global" class="form-control" required>
                <option value="0">否</option>
                <option value="1">是</option>
            </select>
        </div>
    </div>
@endsection

@prepend("script")
<script>
    module.editor.setModel = function (model) {
        $("#alias").val(model.alias);
        $("#link").val(model.link);
        $("#global").val(model.global);
    }
</script>
@endprepend