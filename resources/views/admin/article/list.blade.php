@extends("kiwi::common.list")

@section("title", "文章管理")

@var("model", "article")

@prepend("script")
<script>
    module.table.setPointerFilter("channel", function (callback) {
        create_channel_selector("", callback);
    });
    module.operator.create = function () {
        window.open("/article/edit", "_blank");
    };
    module.operator.update = function (id) {
        window.open("/article/edit?id=" + id, "_blank");
    };
    module.operator.preview = function (id) {
        window.open("/article/preview/" + id + "/1/", "_blank");
    };
    module.operator.previewm = function (id) {
        window.open("/article/preview/" + id + "/2/", "_blank");
    };
    module.operator.publish = function (id) {
        var self = this;
        var d = dialog({
            title: '确认',
            content: '是否发布？',
            okValue: '确定',
            ok: function () {
                d.remove();
                tools.post("/api/article/" + id + "/publish", null, function () {
                    module.table.reload();
                });
            },
            cancelValue: '取消',
            cancel: function () {
                d.remove();
            }
        });
        d.showModal();
    };
    module.operator.manual = function (id) {
        manualList(id)
    };
</script>
@endprepend
@include("kiwi::manualdata.script")
@include("kiwi::channel.tree")
