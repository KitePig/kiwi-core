@extends("kiwi::body")

@section("title", "编辑文章")

@section("body")
    <div class="content">
        <form id="form" class="form-horizontal" role="form">
            {{csrf_field()}}
            <div class="form-group">
                <label for="title" class="control-label col-sm-1">文章标题</label>
                <div class="col-sm-8">
                    <input id="title" name="title" type="text" class="form-control" required maxlength="50">
                </div>
            </div>

            <div class="form-group">
                <label for="short_title" class="control-label col-sm-1">短标题</label>
                <div class="col-sm-8">
                    <input id="short_title" name="short_title" type="text" class="form-control" maxlength="15">
                </div>
            </div>

            <div class="form-group">
                <label for="channel" class="control-label col-sm-1">主頻道</label>
                <div class="col-sm-8">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <a class='btn btn-primary btn-flat channel-select'>选择</a>
                        </span>
                        <select id="channel" name="channel" class="form-control" disabled>
                            @foreach($channels as $channel)
                                <option value="{{$channel->name}}">{{$channel->title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="channels" class="control-label col-sm-1">其他頻道</label>
                <div class="col-sm-8">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <a class='btn btn-primary btn-flat channels-select'>选择</a>
                        </span>
                        <select id="channels" name="channels[]" class="form-control" multiple="multiple"
                                disabled>
                            @foreach($channels as $channel)
                                <option value="{{$channel->name}}">{{$channel->title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="tags" class="control-label col-sm-1">标签</label>
                <div class="col-sm-8">
                    <select id="tags" name="tags[]" class="form-control" multiple="multiple">
                        @foreach($tags as $tag)
                            <option value="{{$tag->name}}">{{$tag->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-1">缩略图</label>
                <input type='file' id='image-file' style='display: none'>
                <input type='hidden' id='image' name='image' required>
                <div class="col-sm-8">
                    <div id='image-button' class='btn btn-primary btn-sm btn-flat'>上传</div>
                    <div id='image-clear-button' class='btn btn-danger btn-sm btn-flat'>清除</div>
                    <br/>
                    <img src='' id='image-image' style='display: none; padding-top: 10px;' width="150"/>
                </div>
            </div>

            <div class="form-group">
                <label for="editor" class="control-label col-sm-1">文章内容</label>
                <div class="col-sm-10">
                    <input id="editor" type="text" name="content" class="form-control" required/>
                </div>
            </div>

            <input type="hidden" name="state" value="1">

            <div class="form-group">
                <div class="col-sm-12">
                    <a id="save" class="btn btn-primary btn-sm btn-flat pull-right margin-r-5" style="position: fixed;right: 20px; bottom: 60px;">保存文章</a>
                    <a id="publish" class="btn btn-primary btn-sm btn-flat pull-right margin-r-5" style="position: fixed;right: 20px; bottom: 20px;">发布文章</a>
                </div>
            </div>
        </form>
    </div>
@endsection

@prepend("script")
<script src="/plugins/ckeditor_4.7.3/ckeditor.js"></script>
<script>
    function initEditor($editor) {
        CKEDITOR.replace('editor', {
            filebrowserImageUploadUrl: '/api/ckeditor/upload',
            height: '700px',
        });
        CKEDITOR.editorConfig = function (config) {
            config.extraPlugins = 'wordcount,notification';
            config.wordcount = {
                showCharCount : true,
                showWordCount : true,
                maxWordCount: 300,
            };
            config.enterMode = CKEDITOR.ENTER_P;
            config.uiColor = '#99FF99';
        };
        CKEDITOR.instances.editor.on("key", function () {
            $editor.val(CKEDITOR.instances.editor.getData());
        });
        CKEDITOR.instances.editor.on("change", function () {
            $editor.val(CKEDITOR.instances.editor.getData());
        });
        CKEDITOR.instances.editor.on("instanceReady", function () {
            CKEDITOR.instances.editor.setData($editor.val());
        });
    }

    function setEditor($editor, content) {
        CKEDITOR.instances.editor.setData(content);
        $editor.val(content);
    }

    var current;

    $(function () {

        var $form = $("#form");

        // channel
        var $channel = $("#channel");
        $channel.select2({});
        $channel.on("select2:opening", function (e) {
            e.preventDefault();
        });
        $(".channel-select").click(function () {
            create_channel_selector($channel.val(), function (name) {
                $channel.val(name).trigger("change");
            });
        });

        // channels
        var $channels = $("#channels");
        $channels.select2({});
        $channels.on("select2:opening", function (e) {
            e.preventDefault();
        });
        $(".channels-select").click(function () {
            create_channels_selector($channels.val(), function (names) {
                $channels.val(names).trigger("change");
            });
        });

        // tags
        var $tags = $("#tags");
        $tags.select2({});

        // image
        tools.initImageInput("image", "/api/file/upload/article");

        // content
        var $editor = $("#editor");
        initEditor($editor);

        var $state = $("input[name=state]");
        $("#save").on("click", function () {
            $state.val(0);
            $form.submit();
        });
        $("#publish").on("click", function () {
            $state.val(1);
            $form.submit();
        });

        var id = tools.getQueryStr("id");
        if (id) {
            tools.get("/api/article/" + id, undefined, function (article) {
                current = article;

                // title
                $("#title").val(article.title);
                $("#short_title").val(article.short_title);

                // channel
                $channel.val(article.channel).trigger("change");
                $channels.val(article.channels).trigger("change");

                // tags
                $tags.val(article.tags).trigger("change");

                // image
                tools.setImageInputValue($("#image"), $("#image-image"), article.image);

                // content
                CKEDITOR.instances.editor.setData(article.content);
                $editor.val(article.content);

                $("#save").remove();
            });

            tools.initAjaxForm($form, "/api/article/" + id + "/update", function () {
                window.close();
            });
        } else {
            tools.initAjaxForm($form, "/api/article/create", function () {
                window.close();
            });
        }
    });
</script>
@endprepend

@include("kiwi::channel.tree")