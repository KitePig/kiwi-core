@extends("kiwi::common.edit")

@section("form")
    <div class="input-group text_title">
        <label for="name">英文名称</label>
        <input id="name" name="name" type="text" class="form-control widthinu" required>
    </div>

    <div class="input-group text_title">
        <label for="title">中文名称</label>
        <input id="title" name="title" type="text" class="form-control widthinu" required>
    </div>
@endsection

@prepend("script")
<script>
    module.editor.setModel = function (tag) {
        $("#name").val(tag.name);
        $("#title").val(tag.title);
    }
</script>
@endprepend