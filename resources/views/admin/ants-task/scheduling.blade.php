@extends("kiwi::index")

@section("title", "蚂蚁全局自动发布中心")

@section("content")
    <div class="row">
        <div class="col-xs-10">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"></h3>
                    <input type="button" value="新建" id="new_ants_published" class="btn btn-info btn-sm" />


                    {{--<div class="box-tools">--}}
                        {{--<div class="input-group input-group-sm" style="width: 150px;">--}}
                            {{--<input type="text" name="table_search" class="form-control pull-right" placeholder="Search">--}}

                            {{--<div class="input-group-btn">--}}
                                {{--<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>ID</th>
                            <th>任务状态</th>
                            <th>任务名称</th>
                            <th>自动计划</th>
                            <th>发布规则</th>
                            <th>排除频道</th>
                            <th>操作</th>
                        </tr>
                        @foreach($scheduling as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>@if($item->state == 1)<span class="label label-info">运行中</span>@else<span class="label label-warning">已关闭</span>@endif</td>
                            <td>{{$item->title}}</td>
                            <td><span class="label label-info">{{$item->auto_published_cron}}</span></td>
                            <td><span class="label label-success" style="cursor: pointer;" title="{{$item->auto_published_rules}}">..</span></td>
                            <td>{{$item->exclude_channels}}</td>
                            <td><span class="label label-danger collect_update" style="cursor: pointer;" data-id="{{$item->id}}">改</span>
                                <span class="label label-danger collect_del" style="cursor: pointer;" data-id="{{$item->id}}">删</span></td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

@endsection
@prepend("script")
<script type="text/html" id="template_collect_published">
    <form id="form_editor_default" class="form-horizontal" role="form">
        <div class="input-group text_title">
            <label for="state">任务状态</label>
            <input id="state" name="state" type="text" class="form-control widthinu" value="2" required>
        </div>
        <div class="input-group text_title">
            <label for="title">任务名字</label>
            <input id="title" name="title" type="text" class="form-control widthinu" required>
        </div>
        <div class="input-group text_title">
            <label for="auto_published_cron">发布计划</label>
            <input id="auto_published_cron" name="auto_published_cron" type="text" class="form-control widthinu" placeholder="* */4 * * * 每四小时执行一次" required>
        </div>
        <div class="input-group text_title">
            <label for="auto_published_rules">发布规则</label>
            <textarea name="auto_published_rules" id="auto_published_rules" placeholder="auto published rules" cols="92" rows="8" required>[
    {
        "type":"article",
        "published_type":"all",
        "published_array":[
            {
                "channel":"1",
                "count":"1"
            }
        ]
    }
]</textarea>
        </div>
        <div class="input-group text_title">
            <label for="exclude_channels">排除频道</label>
            <input id="exclude_channels" name="exclude_channels" type="text" class="form-control widthinu" required>
        </div>
    </form>
</script>
<script>
    $("#new_ants_published").on('click', function () {
        var dlg = dialog({
            title: "新建任务",
            content: $("#" + 'template_collect_published').html(),
            okValue: '确定',
            ok: function () {
                var state = $('#state').val();
                var title = $('#title').val();
                var auto_published_cron = $('#auto_published_cron').val();
                var auto_published_rules = $('#auto_published_rules').val();
                var exclude_channels = $('#exclude_channels').val();
                dlg.remove();
                tools.post('/api/ants/published/create', {
                    'state': state,
                    'title': title,
                    'auto_published_cron': auto_published_cron,
                    'auto_published_rules': auto_published_rules,
                    'exclude_channels': exclude_channels,
                }, function () {
                    location.reload()
                });
            },
            cancelValue: '取消',
            cancel: function () {
                dlg.remove();
            }
        });
        dlg.showModal();
    });


    $(".collect_del").on('click', function () {
        var id = $(this).attr('data-id');
        var dlg = dialog({
            title: "删除任务",
            content: '确认删除任务.',
            okValue: '确定',
            ok: function () {
                dlg.remove();
                tools.get('/api/ants/published/'+id+'/delete', {}, function () {
                    location.reload()
                });
            },
            cancelValue: '取消',
            cancel: function () {
                dlg.remove();
            }
        });
        dlg.showModal();
    })


    $(".collect_update").on('click', function () {
        var id = $(this).attr('data-id');
        tools.get('/api/ants/published/'+id+'/info', {}, function (info) {

            $("#state").val(info.data.state);
            $("#title").val(info.data.title);
            $("#auto_published_cron").val(info.data.auto_published_cron);
            $("#auto_published_rules").val(info.data.auto_published_rules);
            $("#exclude_channels").val(info.data.exclude_channels);
        });

        var dlg = dialog({
            title: "修改任务",
            content: $("#" + 'template_collect_published').html(),
            okValue: '确定',
            ok: function () {
                var state = $('#state').val();
                var title = $('#title').val();
                var auto_published_cron = $('#auto_published_cron').val();
                var auto_published_rules = $('#auto_published_rules').val();
                var exclude_channels = $('#exclude_channels').val();
                dlg.remove();
                tools.post('/api/ants/published/'+id+'/update', {
                    'state': state,
                    'title': title,
                    'auto_published_cron': auto_published_cron,
                    'auto_published_rules': auto_published_rules,
                    'exclude_channels': exclude_channels,
                }, function () {
                    location.reload()
                });
            },
            cancelValue: '取消',
            cancel: function () {
                dlg.remove();
            }
        });
        dlg.showModal();
    });
</script>
@endprepend