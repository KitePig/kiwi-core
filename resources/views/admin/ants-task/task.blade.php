@extends("kiwi::index")

@section("title", "任务详情")

@section("content")
    <div class="row">

    @include('kiwi::ants-task.layout.nav')

    <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="javascript:;">任务配置</a></li>
                    <li><a href="/ants/data/{{$task->id}}">数据采集</a></li>
                    <li><a href="/ants/release/{{$task->id}}">数据发布</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                        <form class="form-horizontal">
                            <div class="form-group has-success">
                                <label for="title" class="col-sm-2 control-label">任务名称</label>

                                <div class="col-sm-4">
                                    <input name="title" type="text" class="form-control" id="title" placeholder="Title"
                                           value="@if(isset($task->title)){{$task->title}}@endif">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="remove_head" class="col-sm-2 control-label">页面HEAD</label>

                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label>
                                            <input type="radio" name="remove_head" value="1"
                                                   @if($task->remove_head == 1) checked @endif> 不删除(UTF-8编码推荐)
                                        </label>
                                        <label>
                                            <input type="radio" name="remove_head" value="2"
                                                   @if($task->remove_head == 2) checked @endif> 删除(GBK编码推荐)
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="image_download" class="col-sm-2 control-label">本地化图片</label>

                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label>
                                            <input type="radio" name="image_download" value="1"
                                                   @if($task->image_download == 1) checked @endif> 本地化
                                        </label>
                                        <label>
                                            <input type="radio" name="image_download" value="2"
                                                   @if($task->image_download == 2) checked @endif> 不本地化
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address" class="col-sm-2 control-label">任务地址</label>

                                <div class="col-sm-6">
                                    <input name="address" type="text" class="form-control" id="address"
                                           placeholder="address"
                                           value="@if(isset($task->address)){{$task->address}}@endif">
                                </div>
                            </div>
                            <div class="form-group has-warning">
                                <label for="paging_address" class="col-sm-2 control-label">分页地址</label>

                                <div class="col-sm-6">
                                    <input name="paging_address" type="text" class="form-control" id="paging_address"
                                           placeholder="paging address"
                                           value="@if(isset($task->paging_address)){{$task->paging_address}}@endif">
                                    <span class="help-block">例子: http://www.xxx.com/xinwen/page/{page}</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="list_range" class="col-sm-2 control-label">列表范围</label>

                                <div class="col-sm-6">
                                    <input name="list_range" type="text" class="form-control" id="list_range"
                                           placeholder="list range"
                                           value="@if(isset($task->list_range)){{$task->list_range}}@endif">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="list_rules" class="col-sm-2 control-label">列表规则</label>

                                <div class="col-sm-6">
                                    <textarea name="list_rules" class="form-control" id="list_rules"
                                              placeholder="list rules"
                                              rows="5">@if(isset($task->list_rules)){{$task->list_rules}}@endif</textarea>
                                    <p><a href="https://www.json.cn" target="_blank">json</a></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="details_range" class="col-sm-2 control-label">详情范围</label>

                                <div class="col-sm-6">
                                    <input name="details_range" type="text" class="form-control" id="details_range"
                                           placeholder="details range"
                                           value="@if(isset($task->details_range)){{$task->details_range}}@endif">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="details_rules" class="col-sm-2 control-label">详情规则</label>

                                <div class="col-sm-6">
                                    <textarea name="details_rules" class="form-control" id="details_rules"
                                              placeholder="list rules"
                                              rows="5">@if(isset($task->details_rules)){{$task->details_rules}}@endif</textarea>
                                    <p><a href="https://www.json.cn" target="_blank">json</a></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="image_attribute" class="col-sm-2 control-label">采集图片属性</label>

                                <div class="col-sm-6">
                                    <input name="image_attribute" type="text" class="form-control" id="image_attribute"
                                           placeholder="image attribute"
                                           value="@if(isset($task->image_attribute)){{$task->image_attribute}}@endif">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="keywords_replace" class="col-sm-2 control-label">关键词替换</label>

                                <div class="col-sm-6">
                                    <textarea name="keywords_replace" class="form-control" id="keywords_replace"
                                              placeholder="keywords replace"
                                              rows="5">@if(isset($task->keywords_replace)){{$task->keywords_replace}}@endif</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-6">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> 请同意 <a href="javascript:;">蚂蚁采集条款</a>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-6">
                                    <button type="button" class="btn btn-info" id="task_submit">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>

@endsection
@prepend("script")
<script>
    $('#task_submit').on('click', function () {
        var data = {
            title: $('input[name="title"]').val(),
            address: $('input[name="address"]').val(),
            paging_address: $('input[name="paging_address"]').val(),
            remove_head: $('input[name="remove_head"]:checked').val(),
            list_range: $('input[name="list_range"]').val(),
            list_rules: $('textarea[name="list_rules"]').val(),
            details_range: $('input[name="details_range"]').val(),
            details_rules: $('textarea[name="details_rules"]').val(),
            image_download: $('input[name="image_download"]:checked').val(),
            image_attribute: $('input[name="image_attribute"]').val(),
            keywords_replace: $('textarea[name="keywords_replace"]').val(),
        };
        tools.post('/api/ants/task/{{$task->id}}/update', data, function (data) {
            // location.reload()
            if (data.code == 200){
                antsSmallBag.alert(data.message);
            } else {
                antsSmallBag.errorAlert(data.message);
            }
        });
    });
</script>
@endprepend
