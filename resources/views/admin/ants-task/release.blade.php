@extends("kiwi::index")

@section("title", "任务详情")

@section("content")
    <div class="row">

        @include('kiwi::ants-task.layout.nav')

        <div class="col-md-9">

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li><a href="/ants/task/{{$task->id}}">任务配置</a></li>
                    <li><a href="/ants/data/{{$task->id}}">数据采集</a></li>
                    <li class="active"><a href="#timeline" data-toggle="tab">数据发布</a></li>

                    <li class="pull-right">
                        <span class="collect_message"></span>
                        <button id="ants_published" type="button" class="btn btn-info">单次数据发布</button>
                    </li>
                </ul>
                <div class="tab-content">
                <div class="active tab-pane" id="activity">
                    <form class="form-horizontal">
                        <div class="form-group has-success">
                            <label for="remove_head" class="col-sm-2 control-label">发布方式</label>

                            <div class="col-sm-6">
                                <div class="checkbox">

                                    <label>
                                        <input type="radio" name="auto_published_type" value="{{\KiwiCore\Model\AntsCollectTask::AUTO_PUBLISHED_CUSTOM}}" @if($task->auto_published_type == \KiwiCore\Model\AntsCollectTask::AUTO_PUBLISHED_CUSTOM) checked @endif> 独立发布
                                    </label>
                                    <label>
                                        <input type="radio" name="auto_published_type" value="{{\KiwiCore\Model\AntsCollectTask::AUTO_PUBLISHED_GLOBAL}}" @if($task->auto_published_type == \KiwiCore\Model\AntsCollectTask::AUTO_PUBLISHED_GLOBAL) checked @endif> 统一发布
                                    </label>
                                    <label>
                                        <input type="radio" name="auto_published_type" value="{{\KiwiCore\Model\AntsCollectTask::AUTO_PUBLISHED_SHUTDOWN}}" @if($task->auto_published_type == \KiwiCore\Model\AntsCollectTask::AUTO_PUBLISHED_SHUTDOWN) checked @endif> 关闭发布
                                    </label>

                                </div>
                            </div>
                        </div>
                        <div class="form-group has-success">
                            <label for="title" class="col-sm-2 control-label">定时表达式</label>

                            <div class="col-sm-4">
                                <input name="auto_published_cron" type="text" class="form-control" id="auto_published_cron" placeholder="auto published cron" value="@if(!empty($task->auto_published_cron)){{$task->auto_published_cron}}@endif">
                            </div>
                            <p><a href="http://www.matools.com/crontab" target="_blank">cron</a></p>
                        </div>
                        <div class="form-group has-success">
                            <label for="title" class="col-sm-2 control-label">发布规则</label>

                            <div class="col-sm-4">
                                <textarea name="auto_published_rules" class="form-control" id="auto_published_rules" placeholder="auto published rules" rows="5">@if(!empty($task->auto_published_rules)){{$task->auto_published_rules}}@else[
{
    "type":"article",
    "published_type":"single",
    "published_array":[
        {
            "channel":"1",
            "count":"1"
        }
    ]
}
]@endif</textarea>
                            </div>
                            <p><a href="https://www.json.cn" target="_blank">json</a></p>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-6">
                                <button type="button" class="btn btn-success" id="ants_published_submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                </div>
            </div>
        </div>

        <div class="col-md-9">

            <div class="nav-tabs-custom">
                <div class="col-sm-6">
                    <div class="btn-group box-footer">
                        <button type="button" class="btn btn-success btn-sm" id="collect_state_complete" value="{{\KiwiCore\Model\AntsCollectData::STATE_COMPLETE}}">采集完成</button>
                        <button type="button" class="btn btn-success btn-sm dropdown-toggle" id="collect_state_complete_sub" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a class="state_change" data-state="{{\KiwiCore\Model\AntsCollectData::STATE_COMPLETE}}">采集完成</a></li>
                            <li class="divider"></li>
                            <li><a class="state_change" data-state="{{\KiwiCore\Model\AntsCollectData::STATE_PUBLISHED}}">已发布</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">

                    <div class="box-footer clearfix">
                        <div class="pull-right" id="data_pagination_info">
                        </div>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="active tab-pane" id="timeline">
                        <div class="">
                            <div class="box-body table-responsive no-padding" style="position: relative; overflow: auto; width: 100%;">
                                <table class="table table-hover" style="width: 1200px;">
                                    <tr>
                                        <th><input type="checkbox" class="batch_all_ids" /></th>
                                        <th>ID</th>
                                        <th>修改</th>
                                        <th>状态</th>
                                        <th>标题</th>
                                        <th>图片</th>
                                        <th>原文地址</th>
                                        <th>内容</th>
                                        <th>采集信息</th>
                                        <th>爬取时间</th>
                                    </tr>
                                    <tbody id="data_list"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin">
                            <li>
                                <select name="batch-operation-value" class="form-control">
                                    <option value="-1">选择操作</option>
                                    <option value="{{\KiwiCore\Model\AntsCollectData::STATE_NO_COMPLETE}}">设置为-待采集</option>
                                    <option value="{{\KiwiCore\Model\AntsCollectData::STATE_COMPLETE}}">设置为-采集成功</option>
                                    <option value="{{\KiwiCore\Model\AntsCollectData::STATE_PUBLISHED}}">设置为-已发布</option>
                                    <option value="{{\KiwiCore\Model\AntsCollectData::STATE_FAIL}}">设置为-采集失败</option>
                                    <option value="100">删除</option>
                                </select>
                            </li>
                        </ul>
                        <ul class="pagination pagination-sm no-margin">
                            <li>
                                <input type="button" id="batch-operation" class="btn btn-warning btn-sm" value="操作" />
                            </li>
                        </ul>
                        <ul class="pagination pagination-sm no-margin pull-right" id="data_pagination">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@prepend("script")
<script>

    // Data Auto Loading
    antsSmallBag.loadingList('/api/ants/data/{{$task->id}}/paging');

    //  保存定时发布数据
    $('#ants_published_submit').on('click', function () {
        var data = {
            auto_published_type: $("input[name='auto_published_type']:checked").val(),
            auto_published_cron: $("input[name='auto_published_cron']").val(),
            auto_published_rules: $("textarea[name='auto_published_rules']").val(),
        };
        tools.post('/api/ants/task/{{$task->id}}/updatePublished', data, function (result) {
            if (result.code == 200){
                antsSmallBag.alert(result.message);
            } else {
                antsSmallBag.errorAlert(result.message);
            }
        });
    });

    // 列表翻页
    $('#data_pagination').on('click', 'li', function () {
        var url = $(this).children('a').attr('data-vel');
        var param = antsSmallBag.parseParams(url);

        antsSmallBag.loadingList('/api/ants/data/{{$task->id}}/paging', param.page, param.prePage);
    });

    // 类型筛选
    $('.state_change').on('click', function () {
        antsSmallBag.changeDataColorState('/api/ants/data/{{$task->id}}/paging', $(this).attr('data-state'));
    });

    // 批量操作文章
    $('#batch-operation').on('click', function () {
        var batch_ids =[];//定义一个数组
        $('input[name="batch_ids"]:checked').each(function(){
            batch_ids.push($(this).val());
        });

        var batch_operation = $('select[name="batch-operation-value"]').val();
        if (batch_operation == '-1'){
            antsSmallBag.alert('请选择操作类型.');
            return;
        }
        if (batch_ids.length == 0){
            antsSmallBag.alert('请点选要操作的数据.');
            return;
        }

        tools.post('/api/ants/data/{{$task->id}}/batchOperation', {
            'batchOperation': batch_operation,
            'batchIds': batch_ids,
        }, function (result) {
            // 展示已采集数据
            antsSmallBag.updateStatistical({{$task->id}});
            antsSmallBag.changeDataColorState('/api/ants/data/{{$task->id}}/paging', {{\KiwiCore\Model\AntsCollectData::STATE_COMPLETE}});
        });
    });

    //  启动单次数据发布
    $('#ants_published').on('click', function () {
        var dlg = dialog({
            title: "单次数据发布",
            content: $("#" + 'template_ants_published').html(),
            okValue: '发布',
            ok: function () {
                var one_published_rules = $("textarea[name='one_published_rules']").val();
                dlg.remove();
                tools.post('/api/ants/published/{{$task->id}}/push', {
                    'rules': one_published_rules,
                }, function (result) {
                    // 提示信息
                    $('.collect_message').html(result.message);

                    if (result.code == 200){
                        // 展示已采集数据
                        antsSmallBag.updateStatistical({{$task->id}});
                        antsSmallBag.changeDataColorState('/api/ants/data/{{$task->id}}/paging', {{\KiwiCore\Model\AntsCollectData::STATE_PUBLISHED}});
                    }
                });
            },
            cancelValue: '取消',
            cancel: function () {
                dlg.remove();
            }
        });
        dlg.showModal();
    });

</script>
<script type="text/html" id="template_ants_published">
    <form id="form_editor_default" class="form-horizontal" role="form">

        <div class="input-group text_title">
            <label for="title">发布规则</label>
            <textarea cols="92" rows="12" name="one_published_rules">[
    {
        "type":"article",
        "published_type":"single",
        "published_array":[
            {
                "channel":"1",
                "count":"1"
            }
        ]
    }
]</textarea>
        </div>

    </form>
</script>
@endprepend
