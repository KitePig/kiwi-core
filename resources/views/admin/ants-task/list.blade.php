@extends("kiwi::index")

@section("title", "任务中心")

@section("content")
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"></h3>
                    <input type="button" value="新建" id="new_ants_collect" class="btn btn-info btn-sm" />
                    <input type="button" value="镜像" id="copy_ants_collect" class="btn btn-success btn-sm" />

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>ID</th>
                            <th colspan="2">任务名称</th>
                            <th>任务状态</th>
                            <th>目标地址</th>
                            <th>自动采集状态</th>
                            <th>自动采集规则</th>
                            <th>自动发布状态</th>
                            <th>自动发布规则</th>
                            <th>图片本地化</th>
                            <th>最后更新时间</th>
                            <th>操作</th>
                        </tr>
                        @foreach($tasks as $task)
                            <tr class="ants_tr_spin">
                                <td><span class="task-list-id btn btn-flat badge bg-blue">{{$task->id}}</span></td>
                                <td><img width="30" class="ants_td_spin" src="{{asset('ants/image/yuan'.($task->id%6).'.png')}}"></td>
                                <td>{{$task->title}}</td>
                                <td>
                                <span class="update_ants_collect" style="cursor: pointer;" title="修改任务状态">
                                    @if($task->state == \KiwiCore\Model\AntsCollectTask::STATE_NORMAL)
                                        <span class="label label-info" data-id="{{$task->id}}">正常</span>
                                    @elseif($task->state == \KiwiCore\Model\AntsCollectTask::STATE_COMPLETE)
                                        <span class="label label-default" data-id="{{$task->id}}">完结</span>
                                    @elseif($task->state == \KiwiCore\Model\AntsCollectTask::STATE_DELETE)
                                        <span class="label label-warning" data-id="{{$task->id}}">已删除</span>
                                    @endif
                                </span>
                                </td>
                                <td>{{$task->address}}</td>
                                <td>
                                    @if($task->auto_collect_type == \KiwiCore\Model\AntsCollectTask::AUTO_COLLECT_CUSTOM)
                                        <span class="label label-info">已启动</span>
                                    @else
                                        <span class="label label-warning">已关闭</span>
                                    @endif
                                </td>
                                <td><span class="badge bg-blue-gradient">{{$task->auto_collect_cron}}</span></td>
                                <td>
                                    @if($task->auto_published_type == \KiwiCore\Model\AntsCollectTask::AUTO_PUBLISHED_CUSTOM)
                                        <span class="label label-info">独立发布</span>
                                    @elseif($task->auto_published_type == \KiwiCore\Model\AntsCollectTask::AUTO_PUBLISHED_GLOBAL)
                                        <span class="label label-success">全局发布</span>
                                    @else
                                        <span class="label label-warning">已关闭</span>
                                    @endif
                                </td>
                                <td><span class="badge bg-blue-gradient">{{$task->auto_published_cron}}</span></td>

                                <td><span class="label label-success">@if($task->image_download == 1)True @else False @endif</span></td>
                                <td>{{$task->updated_at->format('Y-d-m H:i:s')}}</td>
                                <td><a href="/ants/task/{{$task->id}}"><span class="btn btn-lg label label-warning">进入</span></a></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection
@prepend("script")
<script type="text/html" id="template_collect_new">
    <form id="form_editor_default" class="form-horizontal" role="form">
        <div class="input-group text_title">
            <label for="title">任务名字</label>
            <input id="title" name="title" type="text" class="form-control widthinu" required>
        </div>
    </form>
</script>
<script type="text/html" id="template_collect_copy">
    <form id="form_editor_default" class="form-horizontal" role="form">
        <div class="input-group text_title">
            <label for="taskId">镜像任务ID</label>
            <input id="taskId" name="taskId" type="text" class="form-control widthinu" required>
        </div>
    </form>
</script>
<script type="text/html" id="template_collect_update">
    <form id="form_editor_default" class="form-horizontal" role="form">
        <div class="input-group text_title">
            <label for="state">任务状态</label>
            <input id="state" name="state" type="text" class="form-control widthinu" required>
            <p>0=正常, 1=任务完结, 2=删除</p>
        </div>
    </form>
</script>
<script>
    $("#new_ants_collect").on('click', function () {
        var dlg = dialog({
            title: "新建任务",
            content: $("#" + 'template_collect_new').html(),
            okValue: '确定',
            ok: function () {
                var title = $('#title').val();
                dlg.remove();
                tools.post('/api/ants/task/create', {
                    'title': title,
                }, function () {
                    location.reload()
                });
            },
            cancelValue: '取消',
            cancel: function () {
                dlg.remove();
            }
        });
        dlg.showModal();
    });
    $("#copy_ants_collect").on('click', function () {
        var dlg = dialog({
            title: "镜像任务",
            content: $("#" + 'template_collect_copy').html(),
            okValue: '确定',
            ok: function () {
                var taskId = $('#taskId').val();
                dlg.remove();
                tools.post('/api/ants/task/copy', {
                    'taskId': taskId,
                }, function () {
                    location.reload()
                });
            },
            cancelValue: '取消',
            cancel: function () {
                dlg.remove();
            }
        });
        dlg.showModal();
    });
    $(".update_ants_collect").on('click', function () {
        var taskId = $(this).children('span').attr('data-id');
        var dlg = dialog({
            title: "修改任务",
            content: $("#" + 'template_collect_update').html(),
            okValue: '确定',
            ok: function () {
                var state = $('#state').val();
                dlg.remove();
                tools.post("/api/ants/task/"+taskId+"/updateTask", {
                    'state': state,
                }, function () {
                    location.reload()
                });
            },
            cancelValue: '取消',
            cancel: function () {
                dlg.remove();
            }
        });
        dlg.showModal();
    });
</script>
@endprepend
