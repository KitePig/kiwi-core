// debug模式 按钮
$("#debug_button").on('click', function () {
    tools.post('/api/ants/task/debug', {
        debug_address: $('#debug_address').val(),
        debug_remove_head: $("input[name='debug_remove_head']:checked").val(),
        debug_range: $('#debug_range').val(),
        debug_rules: $('#debug_rules').val(),
    }, function (result) {
        console.log(result);
    });
});

// Ants 动起来
$('.ants_spin').hover(function(){
    $(this).addClass("fa-spin");
}, function () {
    $(this).removeClass("fa-spin");
});

// Ants 动起来
$('.ants_tr_spin').hover(function(){
    $(this).find('.ants_td_spin').addClass("fa-spin");
}, function () {
    $(this).find('.ants_td_spin').removeClass("fa-spin");
});

// 列表全选
$('.batch_all_ids').on('click', function () {
    var checked = $(".batch_all_ids").prop('checked');
    $("input[name='batch_ids']").each(function () {
        $(this).prop("checked", checked);
    });
});