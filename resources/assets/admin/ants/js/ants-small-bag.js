'use strict';

var antsSmallBag = {};

// 更新状态
antsSmallBag.updateStatistical = function(taskId){
    tools.get('/api/ants/task/'+taskId+'/info');
    tools.get('/api/ants/task/'+taskId+'/statistical', {}, function (result) {
        var tmp0 = result.data[0]?result.data[0].count:0;
        var tmp1 = result.data[1]?result.data[1].count:0;
        var tmp2 = result.data[2]?result.data[2].count:0;
        var tmp3 = result.data[3]?result.data[3].count:0;
        var tmp100 = tmp0 + tmp1 + tmp2 + tmp3;
        $("#statistical-0").html(tmp0);
        $("#statistical-1").html(tmp1);
        $("#statistical-2").html(tmp2);
        $("#statistical-3").html(tmp3);
        $("#statistical-100").html(tmp100);
    });
};

antsSmallBag.loadingList = function(url, page = 1, prePage = 20){
    $('.ants_data_load').addClass('fa-spin');
    // 数据分页请求
    tools.get(url, {
        'page': page,
        'perPage': prePage,
        'state': $('#collect_state_complete').val()
    }, function (data) {
        if (data['code'] != 200){
            antsSmallBag.errorAlert(data.message);
        }
        var data = data['data'];
        var items = data['data'];

        var data_list = '';
        for (var i = 0; i < items.length; i++){
            data_list += `
            <tr>
            <td><input type="checkbox" name="batch_ids" value="${items[i]['id']}" /></td>
            <td>${items[i]['id']}</td>
            <td><a href="/ants/data/${items[i]['id']}/edit" target="_blank"><span class="fa fa-edit"></span></a></td>
            <td>${items[i]['state'] == 0 ? '<span class="label label-warning">等待采集</span>' : (items[i]['state'] == 1 ? '<span class="label label-success">采集完成</span>' : (items[i]['state'] == 2 ? '<span class="label label-primary">已发布</span>' : '<span class="label label-danger">采集失败</span>'))}</td>
            <td>${items[i]['title']}</td>
            <td><img style="height: 30px;" src="${items[i]['image']}" /></td>
            <td><a href="${items[i]['source_link']}" target="_blank">${items[i]['source_link']}</a></td>
            <td>${items[i]['message']}</td>
            <td>${items[i]['updated_at']}</td>
            </tr>
            `;
        }

        var data_pagination = `
            <li><a data-vel="${data['first_page_url']}" href="javascript:;">首页</a></li>
            <li class="${data['prev_page_url'] ? '' : 'paginate_button previous disabled'}"><a href="javascript:;" data-vel="${data['prev_page_url']}">上一页</a></li>
            <li class="paginate_button active"><a href="javascript:;" data-vel="${data['current_page']}">${data['current_page']}</a></li>
            <li class="${data['next_page_url'] ? '' : 'paginate_button next disabled'}"><a href="javascript:;" data-vel="${data['next_page_url']}">下一页</a></li>
            <li><a href="javascript:;" data-vel="${data['last_page_url']}">尾页</a></li>
        `;

        var data_pagination_info = `<button type="button" class="btn bg-navy btn-xs">共${data.last_page}页 ${data['total']}条数据 每页${data.per_page}条</button>`;

        // 数据追加
        $('#data_list').html(data_list);
        $('#data_pagination').html(data_pagination);
        $('#data_pagination_info').html(data_pagination_info);

        $('.ants_data_load').removeClass('fa-spin');
    });
};

antsSmallBag.changeDataColorState = function(url, state = 100){
    var filter = {
        0: {
            "class": "btn-warning",
            "value": "0",
            "text": "等待采集"
        },
        1: {
            "class": "btn-success",
            "value": "1",
            "text": "采集完成"
        },
        2: {
            "class": "btn-primary",
            "value": "2",
            "text": "已发布"
        },
        3: {
            "class": "btn-danger",
            "value": "3",
            "text": "采集失败"
        },
        100: {
            "class": "btn-info",
            "value": "100",
            "text": "全部数据"
        }
    };

    $('#collect_state_complete').val(filter[state].value);
    $('#collect_state_complete').html(filter[state].text);

    $('#collect_state_complete,#collect_state_complete_sub').removeClass('btn-warning');
    $('#collect_state_complete,#collect_state_complete_sub').removeClass('btn-success');
    $('#collect_state_complete,#collect_state_complete_sub').removeClass('btn-danger');
    $('#collect_state_complete,#collect_state_complete_sub').removeClass('btn-primary');
    $('#collect_state_complete,#collect_state_complete_sub').removeClass('btn-info');

    $('#collect_state_complete,#collect_state_complete_sub').addClass(filter[state].class);


    antsSmallBag.loadingList(url);
};

antsSmallBag.parseParams = function(url) {
    var query = url.split('?')[1] || '';
    if (!query){
        return '';
    }

    var re = /([^&=]+)=?([^&]*)/g,
        decodeRE = /\+/g,
        decode = function (str) { return decodeURIComponent( str.replace(decodeRE, " ") ); };

    let params = {}, e;

    while ( e = re.exec(query) ) params[ decode(e[1]) ] = decode( e[2] );
    return params;
};

antsSmallBag.errorAlert = function (message) {
    var d = dialog({
        title: "错误信息",
        content: message
    });
    d.showModal();
};

antsSmallBag.alert = function (message) {
    var d = dialog({
        title: "提示信息",
        content: message
    });
    d.showModal();
};