"use strict";

$.extend(true, $.fn.dataTable.defaults, {
    searching: true,    // 必须为true，是search函数可以执行的前提
    ordering: false,
    scrollX: true,
    processing: true,
    serverSide: true,
    pageLength: 25,
    language: {
        sProcessing: "正在加载数据...",
        sLengthMenu: "显示_MENU_条 ",
        sZeroRecords: "没有您要搜索的内容",
        sInfo: "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
        sInfoEmpty: "记录数为0",
        sInfoFiltered: "(全部记录数 _MAX_  条)",
        sInfoPostFix: "",
        sSearch: "搜索",
        oPaginate: {
            sFirst: "第一页",
            sPrevious: " 上一页 ",
            sNext: " 下一页 ",
            sLast: " 最后一页 "
        }
    }
});

function DataTable() {
    this.actionCallback = undefined;

    this.table = undefined;
    this.columns = undefined;
}

DataTable.template = {};

// 列表中的操作
DataTable.template.listAction = _.template(
    "<a class='btn btn-primary btn-sm btn-flat dt-action' data-action='{{name}}' data-id='{{id}}' data-data='{{data}}'>{{title}}</a>");

// 列表中的缩略图
DataTable.template.listThumbnail = _.template(
    "<img src='{{url}}' class='dt-thumbnail'/>");

// 文本过滤
DataTable.template.textFilter = _.template(
    "<label class='filter-element' for='{{name}}'>{{title}}</label>" +
    "<input class='form-control filter-element' type='text' name='{{name}}'>");

// 日期过滤
DataTable.template.dateFilter = _.template(
    "<label class='filter-element' for='{{name}}'>{{title}}</label>" +
    "<input class='form-control filter-element input_date start' type='text' name='{{name}}'>至" +
    "<input class='form-control filter-element input_date end' type='text' name='{{name}}'>");

// 日期时间过滤
DataTable.template.datetimeFilter = _.template(
    "<label class='filter-element' for='{{name}}'>{{title}}</label>" +
    "<input class='form-control filter-element input_datetime start' type='text' name='{{name}}'>至" +
    "<input class='form-control filter-element input_datetime end' type='text' name='{{name}}'>");

// 枚举过滤
DataTable.template.enumFilter = _.template(
    "<label class='filter-element' for='{{name}}'>{{title}}</label>" +
    "<select class='form-control filter-element' name='{{name}}'></select>");

// 关联对象过滤
DataTable.template.pointerFilter = _.template(
    "<label class='filter-element' for='{{name}}'>{{title}}</label>" +
    "<input class='form-control filter-element' type='text' name='{{name}}'>" +
    "<a class='btn btn-primary btn-sm btn-flat filter-element filter-select' name='{{name}}'>选择</a>" +
    "<a class='btn btn-primary btn-sm btn-flat filter-element filter-clear' name='{{name}}'>清除</a>");

DataTable.prototype.buildTable = function ($table, ajaxUrl, columns) {
    var self = this;
    self.$table = $table;

    columns.forEach(function (column) {
        column.render = function (data, type, row, meta) {

            // 逐层获取值
            if (column.field) {
                var keys = column.field.split(".");
                for (var i = 0; i < keys.length; ++i) {
                    data = data[keys[i]];
                    if (data === undefined) {
                        return "";
                    }
                }
            }

            if (data === undefined || data === null) {
                return "";
            }

            var name = column.name;

            switch (column.type) {
                case "op":
                    var opList = [];
                    column.target.forEach(function (op) {
                        if (_.indexOf(row[name], op.name) !== -1) {
                            var param = {
                                name: op.name,
                                title: op.title,
                                id: row.DT_RowId,
                                data: (row.DT_RowAttach != undefined) ? row.DT_RowAttach : ""
                            };
                            opList.push(DataTable.template.listAction(param));
                        }
                    });
                    return opList.join("");
                case "bool":
                    if (data === false) {
                        return "否";
                    }
                    if (data === true) {
                        return "是";
                    }
                    return "未知";
                case "date":
                    return data ? moment(data).format("YYYY-MM-DD") : "";
                case "datetime":
                    return data ? moment(data).format("YYYY-MM-DD HH:mm:ss") : "";
                case "enum":
                    if (column.target[data]) {
                        return column.target[data];
                    }
                    return "";
                case "thumbnail":
                    return DataTable.template.listThumbnail({url: data});
                case "json":
                    if (tools.isEmptyObject(data)) {
                        return "";
                    } else {
                        return self.encodeHtml(JSON.stringify(data));
                    }
                case "string":
                    return self.encodeHtml(data);
                default :
                    return data;
            }
        };
    });

    // DataTable构建
    $table.dataTable({
        ajax: ajaxUrl,
        columns: columns
    });

    // 操作点击事件
    $("tbody", $table).on("click", "a", function () {
        var $this = $(this);
        var id = $this.data("id");
        var action = $this.data("action");
        var data = $this.data("data");

        self.actionCallback && self.actionCallback(action, id, data);
    });

    self.table = $table.DataTable();
    self.columns = columns;
};

DataTable.prototype.buildFilter = function ($filter, filters) {
    var self = this;
    self.$filter = $filter;

    if (filters.length === 0) {
        return;
    }

    filters.forEach(function (filter) {
        self.columns.forEach(function (column) {
            if (column.name !== filter.name) {
                return;
            }

            var name = filter.name;
            var type = filter.type ? filter.type : column.type;
            var title = filter.title ? filter.title : column.title;
            var target = filter.target ? filter.target : column.target;

            switch (type) {
                case "op":
                    break;
                case "bool":
                    $filter.append(DataTable.template.enumFilter({name: name, title: title}));
                    tools.initSelect($("select[name=" + name + "]"),
                        [{name: true, title: "是"}, {name: false, title: "否"}], true);
                    break;
                // case "date":
                //     $filter.append(DataTable.template.dateFilter({name: name, title: title}));
                //     break;
                // case "datetime":
                //     $filter.append(DataTable.template.datetimeFilter({name: name, title: title}));
                //     break;
                case "enum":
                    $filter.append(DataTable.template.enumFilter({name: name, title: title}));

                    var list = [];
                    Object.keys(target).forEach(function (value) {
                        list.push({name: value, title: target[value]});
                    });
                    tools.initSelect($("select[name=" + name + "]"), list, true);
                    break;
                case "pointer":
                    $filter.append(DataTable.template.pointerFilter(column));
                    break;
                default :
                    $filter.append(DataTable.template.textFilter(column));
                    break;
            }
        });
    });
    $filter.append('<button class="btn btn-primary btn-sm btn-flat filter-element filter_ok">筛选</button>');
    $filter.append('<button class="btn btn-primary btn-sm btn-flat filter-element filter_cancel">取消筛选</button>');

    // var buildDateTime = function () {
    //     var result = {};
    //     var elements = $(".input_date, .input_datetime", "#" + filterId);
    //     for (var i = 0; i < elements.length; ++i) {
    //         var $element = $(elements[i]);
    //         var name = $element.attr("name");
    //         if (!result[name]) {
    //             result[name] = {};
    //         }
    //         if ($element.hasClass("start")) {
    //             result[name].start = $element.val();
    //         }
    //         if ($element.hasClass("end")) {
    //             if ($element.hasClass(".input_date")) {
    //                 result[name].end = moment($element.val()).endOf("day");
    //             } else {
    //                 result[name].end = $element.val();
    //             }
    //         }
    //     }
    //     return result;
    // };

    $(".filter_ok").click(function () {
        var elements = $("input,select", $filter);
        for (var i = 0; i < elements.length; ++i) {
            var element = elements[i];
            if (!$(element).hasClass("input_date") && !$(element).hasClass("input_datetime")) {
                self.table.column(element.name + ":name").search(element.value);
            }
        }

        // TODO datetime
        // var datetime = buildDateTime();
        // Object.keys(datetime).forEach(function (name) {
        //     var value = datetime[name];
        //     self.table.column(name + ":name").search(
        //         (value.start ? value.start : "") + "," + (value.end ? value.end : ""));
        // });

        self.table.draw();
    });
    $(".filter_cancel").click(function () {
        var elements = $("input,select", $filter);
        for (var i = 0; i < elements.length; ++i) {
            var element = elements[i];
            self.table.column(element.name + ":name").search("");
            $(element).val("");
        }
        self.table.draw();
    });
};

DataTable.prototype.setPointerFilter = function (name, fn) {
    if (!fn) {
        return;
    }
    var self = this;
    var $input = $("input[name=" + name + "]", self.$filter);
    $(".filter-select[name=" + name + "]", self.$filter).on("click", function () {
        fn(function (value) {
            value && $input.val(value);
        });
    });
    $(".filter-clear[name=" + name + "]", self.$filter).on("click", function () {
        $input.val("");
    });
};

/**
 * set ajax url
 * @param url
 */
DataTable.prototype.setUrl = function (url) {
    this.table.ajax.url(url);
};

/**
 * reload
 */
DataTable.prototype.reload = function () {
    this.table.ajax.reload();
};

DataTable.prototype.encodeHtml = function (s) {
    var REGX_HTML_ENCODE = /"|&|"|<|>|[\x00-\x20]|[\x7F-\xFF]|[\u0100-\u2700]/g;
    return (typeof s != "string") ? s :
        s.replace(REGX_HTML_ENCODE,
            function ($0) {
                var c = $0.charCodeAt(0), r = ["&#"];
                c = (c == 0x20) ? 0xA0 : c;
                r.push(c);
                r.push(";");
                return r.join("");
            });
};
