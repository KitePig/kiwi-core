'use strict';

var admin = {};

/**
 * 创建主table
 * @param $table
 * @param columns
 * @param filters
 * @param operator 针对table的各种行为的操作器
 * @param listUrl 获取列表数据的url
 * @returns {DataTable}
 */
admin.createDataTable = function ($table, columns, filters, operator, listUrl) {
    if (!$table) {
        return;
    }

    // model in protocol
    var model = $table.data("model");

    // table id
    var index = $table.data("index");

    // table
    var table = new DataTable();
    table.model = model;
    table.index = index;

    // build table
    table.buildTable($table, listUrl, columns);

    // cell action in table
    table.actionCallback = function (action, id) {
        operator && operator[action] && operator[action](id);
    };

    // build filter
    var $filter = $(".filter[data-index=" + index + "]");
    $filter && filters && table.buildFilter($filter, filters);

    // operator
    if (operator) {
        operator.table = table;
        operator.model = model;
        operator.index = index;
        table.operator = operator;
    }

    // create
    var $create_btn = $(".btn-create[data-index=" + index + "]");
    $create_btn && $create_btn.click(function () {
        operator && operator.create();
    });

    return table;
};

/**
 * 针对model的各种行为的默认操作
 * @param urlFactory
 * @param editor
 */
admin.createOperator = function (urlFactory, editor) {
    var defaultOperator = {

        // id
        index: "",

        // data table
        table: null,

        // editor
        editor: null,

        // data model
        model: "",

        // operator url creator, build "create url", "update url", "remove url"
        urlFactory: null,

        /**
         * 编辑器对话框操作成功后执行的回调的包装
         * 包装了默认操作:刷新table
         * @param callback
         * @returns {*}
         */
        wrapperCallback: function (callback) {
            if (callback) {
                return callback;
            }

            // default
            var self = this;
            return function () {
                self.table && self.table.reload();
            };
        },

        /**
         * 创建model的默认行为:弹出编辑器对话框
         * @param callback
         */
        create: function (callback) {
            this.editor && this.editor.create(this.wrapperCallback(callback));
        },

        /**
         * 更新model的默认行为:弹出编辑器对话框
         * @param id
         * @param callback
         */
        update: function (id, callback) {
            var self = this;
            tools.get(self.urlFactory.info(id), null, function (tag) {
                self.editor && self.editor.create(self.wrapperCallback(callback), tag);
            });
        },

        /**
         * 删除model的默认行为:弹出确认对话框
         * @param id
         * @param callback
         */
        remove: function (id, callback) {
            var self = this;
            var d = dialog({
                title: '确认',
                content: '是否删除？',
                okValue: '确定',
                ok: function () {
                    d.remove();
                    tools.post(self.urlFactory.remove(id), null, self.wrapperCallback(callback));
                },
                cancelValue: '取消',
                cancel: function () {
                    d.remove();
                }
            });
            d.showModal();
        }
    };
    var result = tools.extendObject({}, defaultOperator);
    result.urlFactory = urlFactory;
    result.editor = editor;
    return result;
};

/**
 * 编辑器对话框
 * @param urlFactory
 * @param templateId <script type="text/html">的id
 * @param formId <script type="text/html">中form的id
 */
admin.createEditor = function (urlFactory, templateId, formId) {
    var defaultEditor = {

        // editor template id, <script>
        templateId: "",

        // operator url creator, build "create url", "update url", "remove url"
        urlFactory: null,

        /**
         * 弹出对话框
         * @param success
         * @param modelObject
         */
        create: function (success, modelObject) {
            var self = this;

            var dlg = dialog({
                id: templateId,
                title: "编辑",
                content: $("#" + templateId).html()
            });
            dlg.showModal();

            self.init && self.init();

            var form = $("#" + formId);
            if (modelObject) {
                self.setModel && self.setModel(modelObject);
                tools.initAjaxForm(form, self.urlFactory.update(modelObject.id), function () {
                    dlg.remove();
                    success && success();
                });
            } else {
                tools.initAjaxForm(form, self.urlFactory.create(), function () {
                        dlg.remove();
                        success && success();
                    }
                );
            }
        }
    };
    var result = tools.extendObject({}, defaultEditor);
    result.urlFactory = urlFactory;
    result.templateId = templateId;
    return result;
};

admin.addSiteQueryString = function (url, site) {
    if (site.length == 0){
        return url;
    }
    return tools.addQueryStr(url, "site", site)
};