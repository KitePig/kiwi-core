<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (!function_exists("tableRoute")) {
    function tableRoute($model, $controller)
    {
        Route::group(["prefix" => "api"], function () use ($model, $controller) {
            Route::get("/{$model}s", "$controller@index");
            Route::get("/$model/{id}", "$controller@info")->name("详情{$model}");
            Route::post("/$model/create", "$controller@create")->name("创建{$model}");
            Route::post("/$model/{id}/update", "$controller@update")->name("修改{$model}");
            Route::post("/$model/{id}/delete", "$controller@delete")->name("删除{$model}");
        });
        return Route::get("/{$model}s", "$controller@view");
    }
}

Route::group(["middleware" => ["auth"]], function () {
    Route::get("/", "IndexController@index");
    Route::get('/auth/password/update', 'Auth\PasswordController@editView');
    Route::post('/api/auth/password/update', 'Auth\PasswordController@update')->name("修改密码");

    Route::group(["middleware" => "can:admin"], function (){
        // log
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
        Route::get("/adminlogs", "LogController@view");
        Route::get("/api/adminlogs", "LogController@index");

        //user
        tableRoute("admin", "AdminUserController");
    });

    Route::group(['middleware' => 'can:manager'], function (){

        tableRoute("tdk", "TDKController");
        tableRoute("friendlink", "FriendLinkController");
        tableRoute("aliaslink", "AliasLinkController");
        tableRoute("manualdata", "ManualDataController");
        tableRoute("variable", "VariableController");
    });

    Route::group(['middleware' => 'can:editor'], function (){

        Route::get("/article/edit", "ArticleController@editView");
        Route::get("/article/preview/{id}/{domain}", "ArticleController@preview");
        tableRoute("article", "ArticleController")->name("default");
        tableRoute("tag", "TagController");
        tableRoute("channel", "ChannelController");
        Route::group(["prefix" => "api"], function () {
            // article
            Route::post("/article/{id}/publish", "ArticleController@publish")->name("发布文章");
            Route::get("/article/{id}/recommend", "ArticleController@getRecommend")->name("Get文章推荐");
            Route::post("/article/{id}/recommend", "ArticleController@recommend")->name("文章推荐");

            // channel tree
            Route::get("/channels-tree", "ChannelController@tree");

            // file upload
            Route::post("/file/upload/{type?}/{name?}", "FileController@upload");
            Route::post("/ckeditor/upload", "FileController@ckeditor");

            // manual data list
            Route::get("/manual/{type}/list/{id}", "ManualDataController@manualListByType");
            Route::post("/manual/data/update", "ManualDataController@manualUpdate");


            // Ants Collect
            // 任务详情
            Route::get("/ants/task/{id}/statistical", "AntsCollectTaskController@statistical");
            Route::get("/ants/task/{id}/info", "AntsCollectTaskController@info");
            Route::post("/ants/task/create", "AntsCollectTaskController@create");
            Route::post("/ants/task/copy", "AntsCollectTaskController@copy");
            Route::post("/ants/task/{id}/update", "AntsCollectTaskController@update");
            Route::post("/ants/task/{id}/updateTask", "AntsCollectTaskController@updateTask");
            // *************** //
            Route::post("/ants/task/debug", "AntsCollectTaskController@debug");
            Route::post("/ants/published/create", "AntsCollectPublishedController@create");
            Route::get("/ants/published/{id}/delete", "AntsCollectPublishedController@delete");
            Route::get("/ants/published/{id}/info", "AntsCollectPublishedController@info");
            Route::post("/ants/published/{id}/update", "AntsCollectPublishedController@update");
            Route::post("/ants/published/{id}/push", "AntsCollectPublishedController@push");
            Route::post("/ants/task/{id}/updatePublished", "AntsCollectTaskController@taskUpdatePublished");
            Route::post("/ants/task/{id}/updateCollect", "AntsCollectTaskController@taskUpdateCollect");
            Route::get("/ants/task/{id}/startJob", "AntsCollectTaskController@startJob");
            Route::get("/ants/task/{id}/startJobPaging", "AntsCollectTaskController@startJobPaging");
            Route::get("/ants/task/{id}/startJobDetails", "AntsCollectTaskController@startJobDetails");
            Route::get("/ants/data/{id}/paging", "AntsCollectDataController@paging");
            Route::post("/ants/data/{id}/batchOperation", "AntsCollectDataController@batchOperation");
            Route::post("/ants/data/{id}/update", "AntsCollectDataController@update");
        });
        Route::get("/file/upload/", "FileController@view");

        // Ants Collect
        Route::get("/ants/help", "AntsCollectTaskController@help");
        Route::get("/ants/task", "AntsCollectTaskController@view");
        Route::get("/ants/task/{id}", "AntsCollectTaskController@task");
        Route::get("/ants/data/{id}", "AntsCollectDataController@data");
        Route::get("/ants/data/{id}/edit", "AntsCollectDataController@edit");
        Route::get("/ants/release/{id}", "AntsCollectPublishedController@release");
        Route::get("/ants/published", "AntsCollectPublishedController@view");
    });

});
